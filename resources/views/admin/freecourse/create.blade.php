@extends('layouts.admin')
@section('title','Free Courses')
@section('subTitle','Free Courses')
@section('style')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

@endsection
@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('freecourses.index') }}">Free Courses</a></li>
        <li class="breadcrumb-item active">Create Course</li>
    </ol>
@endsection
@section('content')

    <!-- form start -->
    <form action="{{ route('freecourses.store') }}" method="post" enctype="multipart/form-data" >
        @csrf
        <div class="row">
            <div class="col-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <h5><i class="fas fa-exclamation-triangle"></i> Error:</h5>
                        <ul class="m-1 pl-3">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title bold">Courseing Content</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <label for="txtMentorname">Mentor Name</label>
                            <input id="txtMentorname" type="text" name="mentor_name" value="{{ old('mentor_name') }}" class="form-control @error('mentor_name') is-invalid @enderror border-warning" placeholder="Enter mentor name" >
                            @error('mentor_name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="txtPrice"><b>Title</b></label>
                            <input id="txtPrice" type="text" name="title" value="{{ old('title') }}" class="form-control @error('title') is-invalid @enderror border-warning" placeholder="Enter profession or title" >
                            @error('title')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="txtDesc">Description</label>
                            <textarea id="txtDesc" name="desc" class="form-control @error('desc') is-invalid @enderror" rows="3" placeholder="Enter ..." >{{ old('desc') }}</textarea>
                            @error('desc')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="inpPrimaryPhoto">Primary Photo <small>(.jpg, .jpeg, .png)</small></label>
                            <div class="custom-file">
                                <input value="{{ old('thumbnail_photo') }}" name="thumbnail_photo" type="file" class="custom-file-input @error('thumbnail_photo') is-invalid @enderror" id="inpPrimaryPhoto" accept="image/*"  >
                                <label class="custom-file-label" for="inpPrimaryPhoto">Choose file...</label>
                                @error('thumbnail_photo')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtCourseVideoURL">Highlight Video URL</label>
                            <input id="txtCourseVideoURL" type="text" name="video_url" value="{{ old('video_url') }}" class="form-control @error('video_url') is-invalid @enderror" placeholder="Enter URL" >
                            @error('video_url')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        {{--
                        <div class="form-group">
                            <label for="inpHighlightVideo">Highlight Video <small>(.flv, .mp4, .mov, .avi, .wmv)</small></label>
                            <div class="custom-file">
                                <input value="{{ old('highlight_video') }}" name="highlight_video" type="file" class="custom-file-input @error('highlight_video') is-invalid @enderror" id="inpHighlightVideo" accept="video/*" >
                                <label class="custom-file-label" for="inpHighlightVideo">Choose file...</label>
                                @error('highlight_video')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        --}}
                    </div>
                    <!-- /.card-body -->

                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-12 mb-3">
                <input type="submit" value="Create new Course" class="btn btn-success float-right">
            </div>
        </div>
    </form>

@endsection

@section('script')
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>

    <script>
        $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            $(this).closest('.form-group').find('.custom-file-label').html(fileName);
        });

        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        $('#myform').submit(function() {
            // your code here
        });
    </script>
@endsection