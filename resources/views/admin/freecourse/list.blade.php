@extends('layouts.admin')
@section('title','Free Courses')
@section('subTitle','Free Courses')
@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="../../plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection
@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item active">Free Courses</li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2 mb-md-3">
                        <div class="col-12 d-flex justify-content-between">
                            <div class="left"></div>
                            <div class="right">
                                <a class="btn btn-success btn-sm" href="{{ route('freecourses.create') }}"> Create Free Course</a>
                            </div>
                        </div>
                    </div>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table id="dt1" class="table table-bordered table-hover dt-responsive" width="100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Owner</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($freeCourses as $course)
                                <tr>
                                    <td>
                                        <img class="mb-1" src="{{$course->getFirstMediaUrl('thumbnail-photo') }}" alt="tatler-class-mentor-image" style="height: 100px; object-fit: cover">
                                    </td>
                                    <td><a href="{{ route('freecourses.show',$course->id) }}"></a>{{ $course->title }}</td>
                                    <td>{{ $course->desc }}</td>
                                    <td>{{ $course->mentor_name }}</td>
                                    <td>
                                        <form action="{{ route('freecourses.destroy',$course->id) }}" method="POST" style="text-align: right;">

                                            <a class="btn btn-link text-primary" href="{{ route('freecourses.edit',$course->id) }}"><i class="fas fa-pencil-alt"></i></a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-link text-danger btnDeleteFreecourse"><i class="fas fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>

    $(function () {
        $('#dt1').DataTable({
            "columnDefs": [
                { "orderable": false, "targets": 4 }
            ],
            "scrollX": true
        });
    });


    $(document).ready(function () {
        $(".btnDeleteFreecourse").click(function (e) {
            e.preventDefault();
            if (confirm('Are you sure want to delete this record')) {
                $(this).closest('form').submit();
            } else {
                return false;
            }
        })
    })
</script>
@endsection