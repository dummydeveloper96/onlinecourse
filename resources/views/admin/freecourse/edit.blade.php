@extends('layouts.admin')
@section('title','Free Courses')
@section('subTitle','Free Courses')
@section('style')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <style>
        .img-thumbnail{
            height: 170px;
        }
    </style>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('freecourses.index') }}">Free Courses</a></li>
        <li class="breadcrumb-item active">Update Course</li>
    </ol>
@endsection
@section('content')

    <!-- form start -->
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <form action="{{ route('freecourses.update',$course->id) }}" method="post" enctype="multipart/form-data" >
        @csrf
        @method('PUT')
        <div class="row">
            <div class="col-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <h5><i class="fas fa-exclamation-triangle"></i> Error:</h5>
                        <ul class="m-1 pl-3">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title bold">Courseing Content</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <label for="txtMentorname"><b>Mentor Name</b></label>
                            <input id="txtMentorname" type="text" name="mentor_name" value="{{ old('mentor_name',$course->mentor_name) }}" class="form-control @error('mentor_name') is-invalid @enderror border-warning" placeholder="Enter mentor name" >
                            @error('mentor_name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="txtTitle"><b>Title</b></label>
                            <input id="txtTitle" type="text" name="title" value="{{ old('title',$course->title) }}" class="form-control @error('title') is-invalid @enderror border-warning" placeholder="Enter Profession or title" >
                            @error('title')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="txtDesc">Description</label>
                            <textarea id="txtDesc" name="desc" class="form-control @error('desc',$course->desc) is-invalid @enderror" rows="3" placeholder="Enter ..." >{{ old('desc',$course->desc) }}</textarea>
                            @error('desc')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <img src="{{ $course->getFirstMediaUrl(\App\Freecourse::FREECOURSE_MEDIA_THUMBNAIL) }}" alt="..." class="img-thumbnail">
                        <div class="form-group">
                            <label for="inpThumbnailPhoto">Thumbnail Photo <small>(.jpg, .jpeg, .png)</small></label>
                            <div class="custom-file">
                                <input id="inpThumbnailPhoto" value="{{ old('thumbnail_photo') }}" name="thumbnail_photo" type="file" class="custom-file-input @error('thumbnail_photo') is-invalid @enderror" accept="image/*"  >
                                <label class="custom-file-label" for="inpThumbnailPhoto">Choose file...</label>
                                @error('thumbnail_photo')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>

                        @empty(!$course->video_url)
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe src="{{$course->video_url}}" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        @endempty
                        <div class="form-group">
                            <label for="txtCourseVideoURL">Highlight Video URL</label>
                            <input id="txtCourseVideoURL" type="text" name="video_url" value="{{ old('video_url',$course->video_url) }}" class="form-control @error('video_url') is-invalid @enderror" placeholder="Enter URL" >
                            @error('video_url')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                        {{--
                        @empty(!$courseMedia['highlightVideo'])
                            <!-- 16:9 aspect ratio -->
                            <div class="embed-responsive embed-responsive-16by9">
                                <video class="embed-responsive-item" controls>
                                    <source src="{{$courseMedia['highlightVideo']}}">
                                    Your browser does not support the video tag.
                                </video>
                            </div>
                        @endempty
                        <div class="form-group">
                            <label for="inpHighlightVideo">Highlight Video <small>(.flv, .mp4, .mov, .avi, .wmv)</small></label>
                            <div class="custom-file">
                                <input id="inpHighlightVideo" value="{{ old('highlight_video') }}" name="highlight_video" type="file" class="custom-file-input @error('highlight_video') is-invalid @enderror" accept="video/*" >
                                <label class="custom-file-label" for="inpHighlightVideo">Choose file...</label>
                                @error('highlight_video')
                                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        --}}
                    </div>
                    <!-- /.card-body -->

                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-12 mb-3">
                <input type="submit" value="Update" class="btn btn-success float-right">
            </div>
        </div>
    </form>

@endsection

@section('script')
    <!-- Select2 -->
    <script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>

    <script>

        $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            $(this).closest('.form-group').find('.custom-file-label').html(fileName);
        });

        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    </script>
@endsection