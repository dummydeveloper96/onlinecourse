@extends('layouts.admin')
@section('title','Articles')
@section('subTitle','List Article')
@section('style')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

@endsection
@section('breadcrumb')
    {{-- <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('faq.index') }}">faqs</a></li>
        <li class="breadcrumb-item"><a href="{{ route('faqs.show',$item->user->username) }}">Profile</a></li>
        <li class="breadcrumb-item active">Add Lesson</li>
    </ol> --}}
@endsection
@section('content')
  <div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row mb-2 mb-md-3">
                    <div class="col-12 d-flex justify-content-between">
                        <div class="left"></div>
                        <div class="right">
                            <a class="btn btn-success btn-sm" href="{{ route('news.create') }}"> Create Article</a>
                        </div>
                    </div>
                </div>
                @if (isset($success))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ $success }}
                        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if (!empty($errors->first()))
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      {{ $errors->first() }}
                      <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                @endif
                <table id="dt1" class="table table-bordered table-hover dt-responsive" width="100%">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($news as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>{{ $item->created_at }}</td>
                                <td>
                                    <form action="{{ route('news.destroy',$item->id) }}" method="POST" style="text-align: right;">
                                        @csrf
                                        <a class="btn btn-link text-primary" href="{{ route('news.edit',$item->id) }}"><i class="fas fa-pencil-alt"></i></a>

                                        @method('DELETE')

                                        <button type="submit" class="btn btn-link text-danger"><i class="fas fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>
        </div>
    </div>
  </div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>

    $(function () {
        $('#dt1').DataTable({
            "scrollX": true
        });
    });

</script>
@endsection