@extends('layouts.admin')
@section('title','News')
@section('subTitle','Edit News')

@section('style')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

@endsection
@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('news.index') }}">News</a></li>
    </ol>
@endsection
@section('content')
    @if (!empty($errors->first()))
      <div class="alert alert-danger" role="alert">
          {{ $errors->first() }}
      </div>
    @endif
    <form action="{{route('news.update', $news->id)}}" method="post" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label for="Title">Title</label>
        <input type="text" name="title" id="title" class="form-control" value="{{ $news->title }}">
      </div>
      <div class="form-group">
        <label for="Slug">Slug</label>
        <input type="text" name="slug" id="slug" class="form-control" value="{{ $news->slug }}">
      </div>
      <div class="form-group">
        <label for="Slug">News Summary</label>
        <input type="text" name="news_summary" id="news_summary" class="form-control" value="{{ $news->news_summary }}">
      </div>
      <div class="form-group d-flex flex-column">
        <img id="thumbnail" src="{{ $news->getFirstMediaUrl('news_thumbnail') }}" class="img-fluid" alt="news thumbnail">
        <label for="Slug">News Thumbnail</label>
        <input type="file" name="news_thumbnail" id="news_thumbnail" class="form-control">
      </div>
      <div class="form-group">
        <label for="Content">Content</label>
        <textarea class="form-control" name="content" id="content">
          {{ $news->desc }}
        </textarea>
      </div>
      <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection

@section('script')
<script>
  tinymce.init({
    selector: '#content',
    plugins: 'print preview paste autolink visualblocks image link wordcount',
    toolbar: 'undo redo | styleselect bold italic fontselect fontsizeselect | alignleft aligncenter alignright | bullist numlist | outdent indent',
    content_css: ['//fonts.googleapis.com/css?family=Indie+Flower',
                  '//fonts.googleapis.com/css?family=Open+Sans',
                  'https://fonts.googleapis.com/css?family=Merriweather'],
    font_formats: 'Andale Mono=andale mono,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Terminal=terminal,monaco; Times New Roman=times new roman,times;Cabin=cabin; Open Sans=open sans; Merriweather=merriweather, times;',
    fontsize_formats: '11px 12px 14px 16px 18px 24px 36px 48px',
    image_dimensions: false,
    image_class_list: [
      {title: 'Responsive', value: 'img-fluid'}
    ],
    min_width: 1000,
    height: 1000,
    theme: 'silver',
    mobile: {
      theme: 'mobile',
      plugins: [ 'autosave', 'lists', 'autolink' ],
      toolbar: [ 'undo', 'redo', 'bold', 'italic', 'underline', 'image', 'fontsizeselect']
    },
    view: { title: 'View', items: 'code | visualaid visualblocks | preview' },
    paste_data_images: true
  });
  $("#title").on('input',function(){
      var Text = $(this).val();
      Text = convertToSlug(Text);
      $("#slug").val(Text);
  });
  function convertToSlug(Text)
  {
      return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
        ;
  }
  function readURL (input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#thumbnail').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }
  $('#news_thumbnail').on('change',function(){
      readURL(this);
  })
</script>
@endsection