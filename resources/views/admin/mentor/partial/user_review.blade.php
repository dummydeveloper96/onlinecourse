<style>
    #editUserReview .list-group > div{
        cursor: pointer;
    }
    #editUserReview .list-group > div.active{
        background-color: var(--primary);
        margin: 0 -1.25rem;
        padding-left: 1.25rem;
        padding-right: 1.25rem;
        color: white;
    }
</style>
<section id="editUserReview">
    <form action="{{ route('mentors.select-review',[$mentor->user->username]) }}" method="post">
        @csrf
        <div class="d-flex justify-content-between">
            <div>
                <div class="text-muted">{{!empty($reviews) ? "*Choose one to display" : ''}}</div>
            </div>
            <div><button class="btn btn-primary" id="btnSaveUserReview">Save</button></div>
        </div>
        <div class="list-group">
            @forelse($reviews as $review)
            <div href="#" class="userReviewItem border-bottom py-2">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1 pl-4">
                        <input name="rating[]" class="form-check-input" type="checkbox" value="{{$review->user_id}}" id="chkInvReview{{$review->user_id}}"
                        {{$review->is_show ? "checked" : ''}}>
                        {{ $review->user->name }}
                    </h5>
                    <small>3 days ago</small>
                </div>
                <p class="mb-1">{{ $review->testimonial }}</p>
            </div>
            @empty
                <div href="#" class="userReviewItem border-bottom py-2">
                    <p class="mb-1 text-center">This mentor have not review</p>
                </div>
            @endforelse
        </div>
    </form>
</section>

<script>

    $(document).ready(function () {
        $(".userReviewItem").click(function () {
            jThis = $(this);
            $(".userReviewItem").find("input[type='checkbox']").prop('checked',false);
            jThis.find("input[type='checkbox']").prop('checked',true);
        })
    });
</script>