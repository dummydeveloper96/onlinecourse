<div class="container">
    <div class="summary-desc row">
        <div class="col-xs-12 col-sm-4 mb-5">
          <div class="summary-desc__item">
            <img src="{{asset('/img/icon/education.svg')}}" alt="gurusemua class">
            <div class="summary-desc__item__text">
                <h1>{{ $mentors > 50 ? '50+' : $mentors }}</h1>
                <span class="body-2">Total Pengajar</span>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4 mb-5">
          <div class="summary-desc__item">
            <img src="{{asset('/img/icon/blackboard.svg')}}" alt="gurusemua lesson">
            <div class="summary-desc__item__text">
                <h1>{{ $lessons > 50 ? '50+' : $lessons }}</h1>
                <span class="body-2">Total Video</span>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4 mb-5">
          <div class="summary-desc__item">
            <img src="{{asset('/img/icon/video-player.svg')}}" alt="gurusemua duration">
            <div class="summary-desc__item__text">
                <h1>{{ $minutes > 20 ? '20+' : $minutes }}<small><b>m</b></small></h1>
                <span class="body-2">Rata - Rata Per Video</span>
            </div>
          </div>
        </div>
    </div>
</div>