<div class="mentor__rating-form mb-5">
  <div class="container d-flex flex-column">
    <h1>Beri Penilaian</h1>
  <form action="{{ route('main.mentor.send-rating',$mentor->user->username) }}" method="post" id="formRating">
      <div class="d-flex flex-column">
        <div class="col-xs-12 mb-3">
          <div class='rating-stars text-center'>
            <ul id='stars'>
              <li class='star' title='Poor' data-value='1'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='Fair' data-value='2'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='Good' data-value='3'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='Excellent' data-value='4'>
                <i class='fa fa-star fa-fw'></i>
              </li>
              <li class='star' title='WOW!!!' data-value='5'>
                <i class='fa fa-star fa-fw'></i>
              </li>
            </ul>
          </div>
          <div class="rating-value main-font text-center">
          </div>
        </div>
        <div class="col-xs-12 d-flex justify-content-center">
          <div class="d-flex flex-column" style="max-width: 600px; width: 100%">
            <textarea name="testimonial" id="testimonial" class="form-control"></textarea>
            <button type="submit" class="btn btn--medium">SIMPAN</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

@section('script')
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script>
    $(document).ready(function(){
      var ratingValue = 0;
      /* 1. Visualizing things on Hover - See next part for action on click */
      $('#stars li').on('mouseover', function(){
        var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
      
        // Now highlight all the stars that's not after the current hovered star
        $(this).parent().children('li.star').each(function(e){
          if (e < onStar) {
            $(this).addClass('hover');
          }
          else {
            $(this).removeClass('hover');
          }
        });
        
      }).on('mouseout', function(){
        $(this).parent().children('li.star').each(function(e){
          $(this).removeClass('hover');
        });
      });
      
      
      /* 2. Action to perform on click */
      $('#stars li').on('click', function(){
        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
        var stars = $(this).parent().children('li.star');
        
        for (i = 0; i < stars.length; i++) {
          $(stars[i]).removeClass('selected');
        }
        
        for (i = 0; i < onStar; i++) {
          $(stars[i]).addClass('selected');
        }

        // JUST RESPONSE (Not needed)
        ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
        // var msg = "";
        // if (ratingValue > 1) {
        //     msg = "Thanks! You rated this " + ratingValue + " stars.";
        // }
        // else {
        //     msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
        // }
        // responseMessage(msg);
        
      });
      $('#formRating').on('submit',function (e) {
        e.preventDefault();
        let dataInput = {
          rating : ratingValue,
          _token: "{{ csrf_token() }}",
          testimonial : $('#testimonial').val()
        }
        $.ajax({
          url : $(this).attr('action'),
          data : dataInput,
          type : 'POST',
          dataType : 'json',
          success : function (response) {
            if (response.status === 'error') {
              Swal.fire({
                icon: 'warning',
                title: response.msg,
                showConfirmButton: false,
                timer: 2000
              });
            } else {
              Swal.fire({
                icon: 'success',
                title: response.msg,
                showConfirmButton: false,
                timer: 2000
              });
            }
          },
          error : function (response) {
            let result = JSON.parse(response.responseText);
            Swal.fire({
              icon: 'error',
              title: result.errors.testimonial,
              showConfirmButton: false,
              timer: 2000
            });
          }
        })
      })
    });


    // function responseMessage(msg) {
    //   $('.rating-value').fadeIn(200);  
    //   $('.rating-value').html("<h5>" + msg + "</h5>");
    // }
  </script>
@endsection