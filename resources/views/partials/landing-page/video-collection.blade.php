
<div class="container mb-5">
    <div class="video-collection">
        <h1>Guru Kami</h1>
        <div class="row justify-content-md-center">
            <p class="body-2 col col-md-8 mb-5">Berkomitmen untuk memberikan layanan materi pendidikan terbaik, Guru Semua memiliki para pengajar berkualitas dari berbagai profesi, keahlian dan latar belakang pengalaman lapangan terbaik.</p>
        </div>
        <div class="slider-tablet d-none d-sm-block owl-carousel owl-loaded owl-theme mb-4">
          @foreach ($mentors as $section)
              <div class="row">
                @foreach ($section as $mentor)
                  <div class="col-md-4 video-collection__item mb-4" data-aos-duration="1000">
                    <div class="position-relative">
                        <img class="mb-1" src="{{$mentor->getFirstMediaUrl('primary-photo') ?: asset('/img/mentor-img/mentor-image-small.png')}}" alt="tatler-class-mentor-image" style="height: 220px; object-fit: cover">
                    </div>
                    <a href="{{ url('/mentors',$mentor->user->username) }}" class="body-3">{{ $mentor->user->name }}</a>
                    <label class="label-1">{{ $mentor->profesi }}</label>
                  </div>
                @endforeach
              </div>
          @endforeach
        </div>
        <div class="d-block d-sm-none slider-mobile owl-carousel owl-loaded owl-theme mb-4">
          @foreach ($mentorsMobileView as $mentor)
            <div class="col-md-4 video-collection__item mb-4" data-aos-duration="1000">
              <div class="position-relative">
                  <img class="mb-1" src="{{$mentor->getFirstMediaUrl('primary-photo') ?: asset('/img/mentor-img/mentor-image-small.png')}}" alt="tatler-class-mentor-image" style="height: 220px; object-fit: cover">
              </div>
              <a href="{{ url('/mentors',$mentor->user->username) }}" class="body-3">{{ $mentor->user->name }}</a>
              <label class="label-1">{{ $mentor->profesi }}</label>
            </div>
          @endforeach
        </div>
    </div>
</div>
