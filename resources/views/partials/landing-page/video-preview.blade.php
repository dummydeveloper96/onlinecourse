<div class="video-preview">
    <div class="container">
        <div class="whatis d-flex flex-column my-5 pt-5">
          <h1 class="text-center">Apa Itu Guru Semua?</h1>
          <div class="row justify-content-center">
            <div class="col-xs-12 col-sm-8">
              <p class="body-2 text-center">
                Guru Semua adalah layanan online berbasis pendidikan yang bertujuan untuk memperluas akses dan kesempatan bagi para siswa agar dapat meraih pendidikan serta pembelajaran secara mudah, praktis dan cepat.
              </p>
            </div>
          </div>
          <div class="row justify-content-center">
            <div class="col-xs-12 col-sm-8">
              <iframe class="text-center" style="width: 100%" height="315" src="{{$landingpage->heading_video_yt ?? 'https://www.youtube.com/embed/SIkQ4Q78cVk'}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
        </div>
        {{--    FREE COURSE VIDEO    --}}
        <div class="words-from-expert row my-5">
            <div class="col-xs-12 d-flex flex-column align-self-center mb-3 pt-5">
                <h1 class="text-center">Kata Guru</h1>
                <div class="row justify-content-center">
                  <div class="col-xs-12 col-sm-8">
                    <p class="body-2 text-center">
                      Guru Semua menyediakan berbagai materi pembelajaran dan kisah inspiratif yang bersumber dari para pemimpin atau tokoh yang sudah berkarya serta bergulat panjang di bidangnya masing-masing. Mereka mampu menjadi sumber motivasi, teladan serta memberi pengalaman yang berharga.
                    </p>
                  </div>
                </div>
            </div>
            @php $freecourses = \App\Freecourse::orderBy('updated_at','desc')->get()->chunk(6);; @endphp
            @php $freecoursesMobileView = \App\Freecourse::orderBy('updated_at', 'desc')->get(); @endphp
            @isset($freecourses)
              <div class="slider-tablet owl-carousel owl-theme owl-loaded d-none d-sm-block">
                @foreach ($freecourses as $section)
                  <div class="row">
                    @foreach($section as $freecourse)
                      <div class="col-md-4 free-course video-collection__item mb-4"
                          data-aos-duration="1000">
                          <a href="{{ url('/freecourse',$freecourse->id) }}">
                            <div class="position-relative">
                              <button class="ytp-large-play-button ytp-button" aria-label="Play"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#212121" fill-opacity="0.8"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                                <img class="mb-1 w-100" src="{{$freecourse->getFirstMediaUrl(\App\Freecourse::FREECOURSE_MEDIA_THUMBNAIL) ?: asset('/img/mentor-img/mentor-image-small.png')}}" alt="tatler-class-mentor-image" style="height: 220px; object-fit: cover">
                            </div>
                            <div class="body-3">{{ isset($freecourse->mentor_name) ? $freecourse->mentor_name : $freecourse->title }}</div>
                            <label class="label-1" style="text-transform: none">{{ isset($freecourse->title) ? $freecourse->title: '-' }}</label>
                          </a>
                      </div>
                    @endforeach
                  </div>
                @endforeach
              </div>
              <div class="d-block d-sm-none slider-mobile owl-carousel owl-loaded owl-theme mb-4">
                @foreach ($freecoursesMobileView as $item)
                  <div class="col-md-4 free-course video-collection__item mb-4"
                      data-aos-duration="1000">
                      <a href="{{ url('/freecourse',$item->id) }}">
                        <div class="position-relative">
                          <button class="ytp-large-play-button ytp-button" aria-label="Play"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#212121" fill-opacity="0.8"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                            <img class="mb-1 w-100" src="{{$item->getFirstMediaUrl(\App\Freecourse::FREECOURSE_MEDIA_THUMBNAIL) ?: asset('/img/mentor-img/mentor-image-small.png')}}" alt="tatler-class-mentor-image" style="height: 220px; object-fit: cover">
                        </div>
                        <div class="body-3">{{ isset($item->mentor_name) ? $item->mentor_name : $item->title }}</div>
                        <label class="label-1" style="text-transform: none">{{ isset($item->title) ? $item->title: '-' }}</label>
                      </a>
                  </div>
                @endforeach
              </div>
            @endisset
        </div>
    </div>
</div>