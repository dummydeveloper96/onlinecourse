<div class="trial pt-5 mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mt-5">
                <h1>Coba Gratis</h1>
                <p class="body-2 mb-5">Pendidikan adalah hak setiap individu. Untuk itu di Guru Semua, kami bertujuan menyediakan pendidikan dan materi pembelajaran dari guru-guru terbaik Indonesia yang dapat dicoba para siswa dengan mudah, cepat dan gratis.</p>
                <a href="/register" class="btn--medium">Daftar Sekarang</a>
            </div>
        </div>
    </div>
</div>