
<li class="nav-dropdown__menu">
    <a href="{{ $notification->data['action_url'] }}">{{ isset($notification->data['short_text']) ? $notification->data['short_text'] : 'Someone mention you in the comment' }}</a>
</li>
