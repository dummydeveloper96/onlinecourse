@extends('layouts.app')

@section('content')

<div class="mentor">
   <img src="{{ asset('/img/decoration/header-decoration.png')}}" class="header-decoration">
   <div class="container">
       <div class="row">
           <div class="col-md-7" >
               @if($mentorPilihan)
                   <label class="overline" data-aos="fade-right" data-aos-duration="700">Mentor Pilihan</label>
                   <h1 data-aos="fade-right" data-aos-duration="1000" data-aos-delay="300">{{ $mentorPilihan->user->name }}</h1>
                   <p data-aos="fade-right" data-aos-duration="1000" data-aos-delay="600"class="body-2">{{ $mentorPilihan->desc }}</p>
                   <a data-aos="fade-up" data-aos-duration="1000" data-aos-delay="900" href="{{ url("/mentors/".$mentorPilihan->user->username) }}" class="btn--large">Belajar Sekarang</a>
               @endif
           </div>
       </div>
   </div>
</div>

<div class="container">
  <div class="d-flex flex-column">
    <div class="row mb-4">
      <div class="col-sm-6 offset-sm-6 d-flex justify-content-end">
        <select name="sort_by" id="sort" class="sort-by">
          <option value="null">Sortir</option>
          <option value="terbaru">Terbaru</option>
          <option value="terlaku">Terlaris</option>
          <option value="rating terbaik">Rating Terbaik</option>
        </select>
      </div>
    </div>
    <div class="row">
      @forelse ($mentors as $item)
          <div class="col-xs-12 mentor-item col-md-4 mb-4">
            <a href="{{ url('/mentors',$item->user->username) }}">
              <div class="position-relative">
                <img src="{{ $item->getFirstMediaUrl('primary-photo') ?: asset('/img/mentor-img/mentor-image-small.png') }}" alt="tatler-class-mentor-image" class="mb-1" style="height: 220px; object-fit: cover">
              </div>
              <div class="body-3">{{ $item->user->name }}</div>
              <label class="label-1">{{ $item->profesi }}</label>
            </a>
          </div>
      @empty
          <h3 class="text-center">Belum Ada Mentor</h3>
      @endforelse
    </div>
    {{ $mentors->links() }}
  </div>
</div>

<div class="video-preview">
   <div class="container">
       <div class="row">
            <div class="col-md-6"
                 data-aos="fade-zoom-in"
                 data-aos-easing="ease-in-back"
                 data-aos-duration="1100"
                 data-aos-offset="200">
               <img class="mentor-request__img__main" src="{{ asset('/img/decoration/mentor-req-img.png')}}">
            </div>
            <div class="col-md-6 d-flex flex-column align-self-center"
                 data-aos="fade-zoom-in"
                 data-aos-easing="ease-in-back"
                 data-aos-duration="1100"
                 data-aos-delay="500"
                 data-aos-offset="200">
               <h1>Tidak Menemukan Mentor Favoritmu?</h1>
               <div class="text-right">
                 <a href="{{ route('about-us') }}" class="btn--medium">Hubungi Kami Disini</a>
              </div>
           </div>
       </div>
   </div>
</div>
@guest
  @include('../partials/landing-page/try-trial')
@endguest
@endsection
@section('script')
<script>
  $(document).ready(function () {
    $('#sort').change(function () {
      if ($('#sort').val() !== 'null') {
        // Call AJAX
        window.location.href = "{{ url('/mentors?sort_by=') }}" + $('#sort').val()
      }
    })
  })
</script>
@endsection
