@extends('layouts.app')

@section('content')
  
    <div class="about">
        <img src="{{URL::asset('/img/decoration/header-decoration.png')}}" class="header-decoration">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h1>Tentang Gurusemua</h1><br>
                    <p class="body-2">
                      Guru Semua yang merupakan cita-cita legacy mendiang Ibu Maria Lukito, bertujuan untuk memperluas akses dan kesempatan bagi para siswa untuk meraih pendidikan serta pembelajaran secara mudah praktis & cepat.
                      Hal ini karena pendidikan adalah pintu gerbang bagi setiap orang untuk meraih kesuksesan dalam kehidupan.
                    </p>
                    <p class="body-2">
                      Guru Semua yang memiliki visi utama pada layanan berbasis pendidikan, senatiasa mengembangkan layanan belajar dengan penggunaan teknologi termasuk layanan kelas virtual, video belajar berlangganan serta konten pendidikan lain yang bisa diakses melalui web.
                    </p>
                    <div class="body-2 font-weight-bold">Email: <span>info.gurusemua@gmail.com</span></div>
                </div>
            </div>
        </div>
    </div>
@endsection
