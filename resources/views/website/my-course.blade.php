@extends('layouts.app')

@section('content')

  <div class="my-course container">
    <h1 class="mb-4">Daftar Kelas Anda</h1>
    <div class="row">
      @forelse($mentors as $mentor)
      <div class="col-xs-12 col-sm-4 mb-4">
        <a href="{{ url('/mentors',$mentor->user->username) }}">
          <div class="card-dark .card-horizontal">
            <div class="card-dark-img">
                <img src="{{$mentor->getFirstMediaUrl('primary-photo') ?: asset('/img/mentor-img/mentor-image-small.png')}}" alt="">
            </div>
            <div class="card-dark-body">
              <div class="card-dark-title">{{ $mentor->user->name }}</div>
              <hr>
              <p class="card-dark-text">
                {{ $mentor->desc }}
              </p>
            </div>
          </div>
        </a>
      </div>
      @empty

        <div class="col-xs-12 col-sm-4 mb-4 text-white">
          Anda masih belum enroll Kelas
        </div>
      @endforelse
    </div>
  </div>

@endsection