@extends('layouts.app')

@section('content')
    <div class="faq-detail container d-flex flex-column">
        <div class="page-title">
            <h5>{{ $faq->title }}</h5>
        </div>
        <div class="faq-content d-flex flex-column">
          <div class="col-sm-12 col-md-8">
            {!! $faq->content !!}
          </div>
        </div>
    </div>
@endsection