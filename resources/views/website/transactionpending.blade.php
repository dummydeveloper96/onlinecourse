@extends('layouts.app')

@section('content')
<div class="transaction-success">
  <div class="d-flex flex-column justify-content-center align-items-center text-center">
    <div class="message-wrapper">
      <h1>Transaksi Pending</h1>
      <div class="my-4">
        <i class="fas fa-minus-circle"></i>
      </div>
      <p class="body-2">Segera selesaikan transaksi dengan nomor invoice <b>{{$result->order_id}}</b> untuk dapat menggunakan kelas yang dipilih.</p>
      <p class="body-2">
        Bank:&nbsp;<b class="text-uppercase">{{ $result->va_numbers[0]->bank }}</b><br />
        Nomor Virtual Account: <b>{{ $result->va_numbers[0]->va_number }}</b><br />
        Jumlah Pembayaran: <b>Rp {{ number_format($result->gross_amount, 0) }}</b>
      </p>
      <a class="btn btn--small" target="_blank" href="{{ $result->pdf_url }}">Tata Cara Pembayaran</a>
      </div>
  </div>
</div>
@endsection