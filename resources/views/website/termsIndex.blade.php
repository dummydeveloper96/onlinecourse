@extends('layouts.app')

@section('content')
  <div class="container terms-page">
    <div class="d-flex flex-column">
      <h1 class="page-title">Syarat & Ketentuan</h1>
      <div class="d-flex flex-row flex-wrap">
        @isset($terms)
          @forelse ($terms as $item)
            <div class="col-sm-12 col-md-4 col-lg-3">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title">{{ $item->title }}</h5>
                  <a href="{{ route('termsDetail', $item->id) }}" class="card-link">Baca</a>
                </div>
              </div>
            </div>
            @empty
            <h3>Belum ada konten</h3>
          @endforelse
        @endisset
      </div>
    </div>
  </div>
@endsection
