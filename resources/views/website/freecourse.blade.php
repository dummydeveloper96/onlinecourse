@extends('layouts.app')
@section('style')
    <style>
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="lesson-show mentor-show__video-desc col-md-12 mt-5">
            <h1>{{ $course->mentor_name }}</h1>
            <label class="overline">{{ $course->title }}</label>
        </div>
    </div>

    <div class="container mb-5">
        <div class="lesson-vid">
            <iframe id="movie_player" style="width: 100%; height: 400px;"  src="{{ $course->video_url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
    </div>

    <div class="container lesson-details">
        <div class="row">
            <div class="lesson-details__desc col-md-12">
                <div class="lesson-details__desc__text">
                    <h1>Deskripsi Pelajaran</h1>
                    <p class="body-2">{{ $course->desc }}</p>
                </div>
                <div class="lesson-details__desc__divider mt-4 mb-4"></div>
            </div>

        </div>
    </div>


@endsection
@section('script')
    <script>
        var STATE = {};
        STATE.listComment = [];
        STATE.user = <?= json_encode(Auth::user())  ?>;
        STATE.isRequest = false;

    </script>
    <script src="http://www.youtube.com/player_api"></script>
@endsection

