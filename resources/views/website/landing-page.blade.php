@extends('layouts.app')

@section('content')
    @if(Auth::guest())
    <div class="header">
        <img src="{{URL::asset('/img/decoration/header-decoration.png')}}" class="header-decoration">

        <div class="container">
            <div class="header__left">
                <h1 data-aos="fade-right" data-aos-duration="1000">
                    Belajar Dari Ahlinya
                </h1>
                <br>
                <p class="body-2" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="400">
                  Pendidikan adalah hak setiap individu.
                  Untuk itu di Guru Semua, kami bertujuan menyediakan pendidikan dan materi pembelajaran dari guru-guru terbaik Indonesia yang dapat diakses para siswa dengan mudah, cepat dan tak dibatasi waktu serta terhalang oleh jarak.
                </p>
                <br>
                @guest
                  <div class="header__signup" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="800">
                      <form method="POST" action="{{ route('login') }}">
                          @csrf
                          <input type="email" name="email" class="header__input" placeholder="Email">
                          <input type="password" name="password" class="header__input" placeholder="Password">
                          <button class="btn--large">Login Sekarang</button>
                      </form>
                  </div>
                @endguest
            </div>
        </div>
    </div>
    <div class="my-4">
      <div class="header__scroll">
        <div class="header__scroll-icon">
            <span>BROWSE CLASS</span>
            <img src="{{URL::asset('/img/icon/scroll-mouse.png')}}">
        </div>
    </div>
    </div>
    @include('../partials/landing-page/summary-desc',['mentors' => $data['mentorCount'],'lessons' => $data['lessonCount'], 'minutes' => $data['avgDuration']])
    @else

        <div class="header pb-0">
            <img src="{{URL::asset('/img/decoration/header-decoration.png')}}" class="header-decoration">

            <div class="container">
                <div class="header__left">
                    <h1 data-aos="fade-right" data-aos-duration="1000">
                        Belajar Dari Ahlinya
                    </h1>
                    <br>
                    <p class="body-2" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="400">
                      Pendidikan adalah hak setiap individu.
                      Untuk itu di Guru Semua, kami bertujuan menyediakan pendidikan dan materi pembelajaran dari guru-guru terbaik Indonesia yang dapat diakses para siswa dengan mudah, cepat dan tak dibatasi waktu serta terhalang oleh jarak.
                    </p>
                    <br>
                    <div class="mt-5"></div>
                    @include('../partials/landing-page/summary-desc',['mentors' => $data['mentorCount'],'lessons' => $data['lessonCount'], 'minutes' => $data['avgDuration']])
                </div>
                <div class="header__scroll">
                    <div class="header__scroll-icon">
                        <span>BROWSE CLASS</span>
                        <img src="{{URL::asset('/img/icon/scroll-mouse.png')}}">
                    </div>
                </div>
                <div class="mb-5"></div>
            </div>
        </div>

    @endif


    @include('../partials/landing-page/video-preview',['landingpage'=>$data['landingpage']])

    @include('../partials/landing-page/video-collection',['mentors' => $data['mentors'], 'mentorsMobileView' => $data['mentorsMobileView']])
    
    @php $testimonials = [
      ['sortDesc' => 'Nambah Wawasan!','nama' => 'Ikhsan Fadillah','testimonial' => 'Terimakasih gurusemua.com yang telah membuat wawasan makeup saya bertambah. Karena kelas Archangela Chelsea sekarang saya lebih semangat untuk menekuni bidang makeup artist'],
      ['sortDesc' => 'Praktis!','nama' => 'M. Rizky','testimonial' => 'Apa yang diajarkan elyas di platform gurusemua sungguh praktis dan mudah dipahami, elyas mengajarkan dunia barista secara bertahap yang membuat saya lebih mudah memahaminya'],
      ['sortDesc' => 'Ringkas dan Padat','nama' => 'Rizmi Danu','testimonial' => "Materi yang terdapat di gurusemua cukup ringkas dan padat. Selain itu, materi yang diberikan pun to the point dan tidak bertele-tele. interface yang gurusemua miliki juga simple dan mudah di gunakan "]
]   @endphp
    <div class="container mb-5" data-aos="fade-up" data-aos-duration="1000" data-aos-offset="250">
      <h1 class="mb-4 text-center pt-5">Kata Mereka</h1>
      <div class="row">
        @foreach ($testimonials as $item)
          <div class="col-sm-4 testimonial__item mb-3">
              <div class="testimonial__text__container">
                  <div class="testimonial__item__text">
                      <h4>{{ $item['sortDesc'] }}</h4>
                      <p>{{ $item['testimonial'] }}</p>
                      <span class="testimonial__item__avatar">{{ $item['nama'] }}</span>
                  </div>
              </div>
          </div>
        @endforeach
      </div>
    </div>

    <div class="row justify-content-md-center d-none">
        <a href="{{ url('/mentors') }}" class="btn--medium">Lihat Semua Mentor</a>
    </div>
    @guest
      @include('../partials/landing-page/try-trial')
    @endguest
@endsection