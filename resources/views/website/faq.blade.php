@extends('layouts.app')

@section('content')
    <div class="notification container d-flex flex-column">
        <div class="page-title">
            <h1>Pertanyaan Umum</h1>
        </div>
        <div class="faqs d-flex flex-column">
            @forelse ($faqs as $item)
                <div class="d-flex flex-row flex-wrap">
                  <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="card">
                      <div class="card-body"></div>
                      <h5 class="card-title main-font">{{ $item->title }}</h5>
                      <a href="{{ route('faq-detail', $item->id) }}" class="card-link">Baca</a>
                    </div>
                  </div>
                </div>
            @empty
                <h3>Belum ada konten</h3>
            @endforelse
        </div>
    </div>
@endsection