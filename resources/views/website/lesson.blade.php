@extends('layouts.app')
@section('style')
    <style>
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="lesson-show mentor-show__video-desc col-md-12 mt-5">
            <label class="overline">{{ $mentor->profesi }}</label>
            <h1>{{ $lesson->title }}</h1>
            <p class="body-2">{{ $lesson->desc }}</p>
        </div>
    </div>
    
    <div class="container mb-5">
        <div class="lesson-vid">
            {{--
            <video width="100%" height="" controls>
                <source src="{{ $lesson->getFirstMediaUrl('lesson-video') }}" type="video/mp4">
                Your browser does not support the video tag.
            </video>
            --}}

            @if(Auth::user()->hasMentor($mentor->mentor_id))
                <div id="myDiv" style="position:relative;" data-lesson_video_dacast_id="{{$lesson->lesson_video_dacast_id}}"></div>
            @else
                <h3 class="text-center text-white">Anda harus berlangganan terlebih dahulu untuk mengakses pelajaran ini</h3>
            @endif
        </div>
    </div>

    <div class="container lesson-details">
        <div class="row">
            <div class="lesson-details__desc col-md-7">
                <div class="lesson-details__desc__text">
                    <h1>Deskripsi Pelajaran</h1>
                    <p class="body-2">{{ $lesson->desc }}</p>
                </div>
                <div class="lesson-details__desc__divider mt-4 mb-4"></div>
                <div class="lesson-details__desc__comment">
                    <div class="lesson-details__desc__comment__title color__white mb-3">Komentar</div>
                    @if(!Auth::user()->isTrial() || Auth::user()->hasMentor($mentor->mentor_id))
                        <div class="mentor__comment-form mb-5">
                            <form action="{{ route('main.mentors.lessons.send-comment',[$mentor->user->username, $lesson->slug]) }}" method="post" class="formComment" data-task="reply">
                                @csrf
                                <input class="txtParentId" type="hidden" name="parent_id" value="0">
                                <input class="txtReplyTo" type="hidden" name="reply_to" value="0">
                                <textarea class="mb-2 txtComment form-control" style="border-radius: 3px" name="text"></textarea>
                                <button class="btnSendComment btn btn--medium btn-block">Kirim</button>
                            </form>
                        </div>
                        <div class="lesson-details__desc__comment__list" id="commentContainer">
                        <div class="text-center " style="font-size: 4em">
                            <i class="fa fa-spin fa-pulse fa-spinner"></i>
                        </div>
                    </div>
                    @else
                        <p class="text-white">Fitur komentar dinonaktifkan. Segera berlangganan agar dapat menikmati fitur ini</>
                    @endif
                </div>
            </div>
                
            <div class="lesson-details__desc col-md-5">
                <div class="lesson-details__desc__mentor">
                    <div class="lesson-details__desc__mentor__details">
                        <div class="lesson-details__desc__mentor__details__avatar">
                            <img src="{{ asset($mentor->getFirstMediaUrl('primary-photo') ?: '/img/mentor-img/mentor-image-small.png') }}" alt="tatler-class-mentor-image" style="height: 80px; object-fit: cover">
                        </div>
                        <div class="lesson-details__desc__mentor__details__text">
                            <h2 class="color__black_primary"><a style="color: #000;" href="{{ url('/mentors',[$mentor->user->username]) }}">{{ $mentor->user->name }}</a></h2>
                            <div class="lesson-details__desc__mentor__details__text__title text-capitalize">
                                {{ $mentor->profesi }}
                            </div>
                        </div>
                    </div>
                    <p class="body-2 color__black_secondary">{{ $mentor->desc }}</p>

                    <form action="{{ route('carts.store') }}" method="POST" class="ml-2">
                        @csrf
                        <input type="hidden" name="cartable_id" value="{{$mentor->mentor_id}}">
                        <input type="hidden" name="cartable_type" value="{{\App\Wishlist::TYPE_MENTOR}}">
                        @if(!Auth::user()->hasMentor($mentor->mentor_id))
                        <button class="btn--medium btn btn-block"><i class="fas fa-cart-plus"></i> Beli Kelas</button>
                        @endif
                    </form>
                </div>
                <div class="lesson-details__desc__navigation">
                    <h3 class="color__black_primary">Daftar Pelajaran</h3>
                    @foreach ($mentor->lessons as $i => $mentorLesson)
                        <a class="color__black_primary" href="{{ route('main.mentors.lessons.show',[$mentor->user->username,$mentorLesson->slug]) }}">{{$i+1}}. {{ $mentorLesson->title }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>


    @if(!Auth::user()->isTrial())
    <div class="container text-white">
        <ul id="commentContainer">
        </ul>
    </div>
    @endif

@endsection
@section('script')
    <script src="https://player.dacast.com/js/player.js?contentId={{ $lesson->lesson_video_dacast_id }}; ?>"></script>
    <script>
        var STATE = {};
        STATE.URL_FETCH_COMMENT = "{{ route('main.mentors.lessons.fetch-comment',[$mentor->user->username, $lesson->slug]) }}";
        STATE.URL_EDIT_COMMENT = "{{ route('main.mentors.lessons.edit-comment',[$mentor->user->username, $lesson->slug]) }}";
        STATE.URL_SEND_COMMENT = "{{ route('main.mentors.lessons.send-comment',[$mentor->user->username, $lesson->slug]) }}";
        STATE.URL_DELETE_COMMENT = "{{ route('main.mentors.lessons.delete-comment',[$mentor->user->username, $lesson->slug]) }}";
        STATE.URL_FINISH_WATCHING = "{{ route('main.mentors.lessons.finish-watching',[$mentor->user->username, $lesson->slug]) }}";
        STATE.listComment = [];
        STATE.user = <?= json_encode(Auth::user())  ?>;
        STATE.isRequest = false;

        window.addEventListener("load", function(){

            var CONTENT_ID = $("#myDiv").data('lesson_video_dacast_id');
            var myPlayer = dacast(CONTENT_ID, 'myDiv', {
                height: 400
            });
            myPlayer.on("complete", function(){
                $.ajax({
                    url : STATE.URL_FINISH_WATCHING,
                    type : 'POST',
                    data : {
                        _token: "{{ csrf_token() }}",
                    },
                    dataType : "json",
                    success(res){
                        console("Video Selesai Di Tonton!");
                    },
                });
            });
        });

        $(document).ready(function () {
            var commentContainer = $('#commentContainer');
            $("#commentContainer").on('click','.btnReply',function () {
                var jThis = $(this);
                const parent_id = jThis.data('parentId');
                const reply_to = jThis.data('replyTo');
                jThis.closest('.button-container').next().html(`
                    <form action="${STATE.URL_SEND_COMMENT}" method="post" class="formComment" data-task="reply">
                        <input class="txtParentId" type="hidden" name="parent_id" value="${parent_id}">
                        <input class="txtReplyTo" type="hidden" name="reply_to" value="${reply_to}">
                        <textarea class="mb-2 txtComment form-control border-none" style="border-radius: 3px" name="text" ></textarea>
                        <button class="btnCancelEdit btn btn-secondary btn-sm">Cancel</button>
                        <button class="btnSendComment btn btn--small">Kirim</button>
                    </form>
                `)
            });
            $("#commentContainer").on('click','.btnEdit',function () {
                var jThis = $(this);
                const id = jThis.data('id');
                let text = jThis.data('text');
                jThis.closest('.button-container').hide();
                jThis.closest('.button-container').next().html(`
                    <form action="${STATE.URL_EDIT_COMMENT}" method="post" class="formComment form mt-3" data-task="edit">
                        <input class="txtId" type="hidden" name="id" value="${id}">
                        <textarea class="mb-2 txtComment form-control border-none" style="border-radius: 3px" name="text">${text}</textarea>
                        <button class="btnCancelEdit btn btn-secondary btn-sm">Cancel</button>
                        <button class="btnSendComment btn btn--small">Kirim</button>
                    </form>
                `)
            });
            $("#commentContainer").on('click','.btnCancelEdit',function (e) {
                e.preventDefault();
                var jThis = $(this);
                jThis.closest('.single-comment-wrapper').find('.button-container').show();
                jThis.closest('.formComment').remove();
            });

            $("#commentContainer").on('click','.btnDelete',function () {
                var jThis = $(this);
                const id = jThis.data('id');
                $.ajax({
                    url : STATE.URL_DELETE_COMMENT,
                    method: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id : id,
                    },
                    dataType: "json",
                }).done(function(msg) {
                    // loading end
                    fetchComment();
                }).fail(function( jqXHR, textStatus ) {
                    alert( "Request failed: " + textStatus );
                });
            });
            $(document).on('click','.btnSendComment',function (event) {
                event.preventDefault();
                var jThis = $(this)
                let dataRequest = {
                    _token: "{{ csrf_token() }}",
                    mentor_id : "{{ $mentor->mentor_id }}",
                };
                var form = $(this).closest('form');
                const task = form.data('task');
                if (task == "reply") {
                    // console.log(form.find(".txtParentId"));
                    dataRequest.parent_id = form.find(".txtParentId").val();
                    dataRequest.reply_to = form.find(".txtReplyTo").val();
                } else if(task == "edit") {
                    dataRequest.id = form.find(".txtId").val();
                }
                dataRequest.text = form.find(".txtComment").val();

                // loading start
                if (STATE.isRequest == true){
                    // console.log(true);
                    return false;
                }

                STATE.isRequest = true;
                jThis.attr("disabled", true);

                $.ajax({
                    url : form.attr('action'),
                    method: "POST",
                    data: dataRequest,
                    dataType: "json",
                }).done(function(msg) {
                    // loading end
                    setTimeout(function () {
                        STATE.isRequest = false;
                    },2000);
                    STATE.isRequest = false;
                    form.find(".txtComment").val('');
                    fetchComment();
                }).fail(function( jqXHR, textStatus ) {
                    alert( "Request failed: " + textStatus );
                }).always(function () {
                    jThis.attr("disabled", false);
                });
            });

            function fetchComment() {
                $.ajax({
                    url: STATE.URL_FETCH_COMMENT,
                    method : 'get',
                    dataType : 'json',
                }).done(function (res) {
                    STATE.listComment = res.comments;
                    renderComment(res.comments);
                    // renderComment(comments);
                }).fail(function( jqXHR, textStatus ) {
                    alert( "Request failed: " + textStatus );
                });
            };

            function list_to_tree(list) {
                var map = {}, node, roots = [], i;
                for (i = 0; i < list.length; i += 1) {
                    map[list[i].id] = i; // initialize the map
                    list[i].children = []; // initialize the children
                }
                for (i = 0; i < list.length; i += 1) {
                    node = list[i];
                    if (node.parent_id != 0) {
                        // if you have dangling branches check that map[node.parent_id] exists
                        list[map[node.parent_id]].children.push(node);
                    } else {
                        roots.push(node);
                    }
                }
                return roots;
            }

            function renderComment(comments) {
                let commentsHTML = "";
                let tree = list_to_tree(comments);
                defaultImg = '{!! asset('/img/mentor-img/mentor-image-small.png') !!}';
                tree.forEach((comment,i) => {
                    commentsHTML += `

                    <div style="${comment.deleted_at == null ? 'display: block' : 'display: none'};">
                        <div class="lesson-details__desc__comment__list__detail single-comment-wrapper" id="comment${comment.id}">
                            <div class="lesson-details__desc__comment__list__detail__avatar">
                                <img src="${comment.user_avatar != null ? comment.user_avatar : defaultImg}" alt="tatler-class-mentor-image" style="height: 40px; object-fit: cover">
                            </div>
                            <div class="lesson-details__desc__comment__list__detail__text">
                                <div class="lesson-details__desc__comment__list__detail__text__name color__white">
                                    ${comment.name} <small class="text-muted" style="font-size: .6em">${timeSince(comment.updated_at)}</small>
                                </div>
                                <div class="lesson-comment">
                                    <p class="body-2"><span class="text-major">@${comment.username}</span> <span class="comment-text">${comment.text}</span></p>
                                    ${comment.user_id == STATE.user.id ? `
                                        <div class="button-container">
                                            <span class="btnReply text-muted btn-link pr-3 text-capitalize d-inline-block" data-reply-to="${comment.id}" data-parent-id="${comment.parent_id}">reply</span>
                                            <span class="btnEdit text-muted btn-link pr-3 text-capitalize d-inline-block" data-id="${comment.id}" data-text="${comment.text}">edit</span>
                                            <span class="btnDelete text-muted btn-link pr-3 text-capitalize d-inline-block" data-id="${comment.id}" >delete</span>
                                        </div>` : `
                                        <div class="button-container">
                                            <span class="btnReply text-muted btn-link pr-3 text-capitalize d-inline-block" data-reply-to="${comment.id}" data-parent-id="${comment.parent_id}">reply</span>
                                        </div>`
                                    }
                                    <div class="form-container form mt-3"></div>
                                </div>
                            </div>
                        </div>`;
                        if (comment.children.length > 0) {
                            commentsHTML += `<ul>`;
                            commentsHTML += renderChild(comment.children);
                            commentsHTML += `</ul>`;
                        }
                    commentsHTML += `</div>`;
                });
                commentContainer.html(commentsHTML);
            }

            fetchComment();


        });

        function renderChild(nodes,HTML = "") {
            defaultImg = '{!! asset('/img/mentor-img/mentor-image-small.png') !!}';
            nodes.forEach((node,i) => {
                HTML += `
                    <div style="${node.deleted_at == null ? 'display: block' : 'display: none'};">
                        <div class="lesson-details__desc__comment__list__detail single-comment-wrapper" id="comment${node.id}">
                            <div class="lesson-details__desc__comment__list__detail__avatar">
                                <img src="${node.user_avatar != null ? node.user_avatar : defaultImg}" alt="tatler-class-mentor-image" style="height: 40px; object-fit: cover">
                            </div>
                            <div class="lesson-details__desc__comment__list__detail__text">
                                <div class="lesson-details__desc__comment__list__detail__text__name color__white">
                                    ${node.name} <small class="text-muted" style="font-size: .6em">${timeSince(node.updated_at)}</small>
                                </div>
                                <div class="lesson-comment">
                                    <p class="body-2"><span class="text-major">@${node.username}</span> <span class="comment-text">${node.text}</span></p>
                                    ${node.user_id == STATE.user.id ?`
                                    <div class="button-container">
                                        <span class="btnReply text-muted btn-link pr-3 text-capitalize d-inline-block" data-reply-to="${node.id}" data-parent-id="${node.parent_id}">reply</span>
                                        <span class="btnEdit text-muted btn-link pr-3 text-capitalize d-inline-block" data-id="${node.id}" data-text="${node.text}">edit</span>
                                        <span class="btnDelete text-muted btn-link pr-3 text-capitalize d-inline-block" data-id="${node.id}">delete</span>
                                    </div> ` : `
                                    <div class="button-container">
                                        <span class="btnReply text-muted btn-link pr-3 text-capitalize d-inline-block" data-reply-to="${node.id}" data-parent-id="${node.parent_id}">reply</span>
                                    </div>`}
                                    <div class="form-container form mt-3"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                if (node.children.length > 0){
                    HTML += renderChild(node.children,HTML);
                }
            });
            return HTML;
        }
        function timeSince(date) {
            var date = new Date(date);
            var seconds = Math.floor((new Date() - date) / 1000);

            var interval = Math.floor(seconds / 31536000);

            if (interval > 1) {
                return interval + " years ago";
            }
            interval = Math.floor(seconds / 2592000);
            if (interval > 1) {
                return interval + " months ago";
            }
            interval = Math.floor(seconds / 86400);
            if (interval > 1) {
                return interval + " days ago";
            }
            interval = Math.floor(seconds / 3600);
            if (interval > 1) {
                return interval + " hours ago";
            }
            interval = Math.floor(seconds / 60);
            if (interval > 1) {
                return interval + " minutes ago";
            }
            return Math.floor(seconds) + " seconds ago";
        }
    </script>
    <script src="http://www.youtube.com/player_api"></script>
@endsection

