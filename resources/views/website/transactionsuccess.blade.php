@extends('layouts.app')

@section('content')
<div class="transaction-success">
  <div class="d-flex flex-column justify-content-center align-items-center text-center">
    <div class="message-wrapper">
      <h1>Transaksi Berhasil</h1>
      <div class="my-4">
        <i class="fas fa-check"></i>
      </div>
      <p class="body-2">Terima kasih telah melakukan enroll kelas di gurusemua. Untuk melihat invoice kamu, bisa dicek di halaman <a class="body-2" href="{{ route('main.user.profile', Auth::user()->username) }}#order"><u>Riwayat Belanja</u></a>.</p>
    </div>
  </div>
</div>
@endsection