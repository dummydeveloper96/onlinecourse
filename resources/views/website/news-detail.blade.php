@extends('layouts.app')

@section('content')

  <div class="news mb-5">
    <img src="{{URL::asset('/img/decoration/header-decoration.png')}}" class="header-decoration">
    <div class="container">
      <div class="banner-article">
        <h1>{{ $news->title }}</h1>
      </div>
    </div>
  </div>
  <div class="container news-detail d-flex flex-column">
    <div class="author">
      {{ ucwords($news->user->name) }}
    </div>
    <div class="upload-date">
      {{ Carbon\Carbon::parse($news->created_at)->format('d M Y') }}
    </div>
    <div class="body secondary-font">
      {!! $news->desc !!}
    </div>
  </div>

@endsection
