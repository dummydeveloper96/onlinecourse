@extends('layouts.app')

@section('content')

  <div class="privacy-detail">
    <div class="container">
      <div class="page-title">
        <h1>{{ $term->title }}</h1>
      </div>
    </div>
  </div>
  <div class="container news-detail d-flex flex-column">
    <div class="author">
      {{ ucwords($term->user->name) }}
    </div>
    <div class="upload-date">
      {{ Carbon\Carbon::parse($term->created_at)->format('d M Y') }}
    </div>
    <div class="body secondary-font">
      {!! $term->content !!}
    </div>
  </div>

@endsection
