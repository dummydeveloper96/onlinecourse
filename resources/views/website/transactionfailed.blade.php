@extends('layouts.app')

@section('content')
<div class="transaction-failed">
  <div class="d-flex flex-column justify-content-center align-items-center text-center">
    <div class="message-wrapper">
      <h1>Transaksi Gagal</h1>
      <div class="my-4">
        <i class="fas fa-times-circle"></i>
      </div>
      <p class="body-2">Mohon maaf transaksi anda gagal dilakukan. Mohon coba beberapa saat lagi.</p>
      <p class="body-2">Jika ada pertanyaan, silahkan hubungi kami di <a href="mailto:gurusemua89@gmail.com" class="ml-3 btn--medium"><i class="fas fa-envelope mr-3"></i>&nbsp;gurusemua89@gmail.com</a></p>
    </div>
  </div>
</div>
@endsection