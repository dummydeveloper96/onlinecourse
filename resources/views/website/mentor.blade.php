@extends('layouts.app')
@section('content')

    <div class="mentor-show">
        <div class="container">
            <div class="row">
                <div class="mentor-show__video-preview col-lg-7 col-md-12">
                   {{-- <video width="100%" height="" controls>
                       <source src="{{ $mentor->getFirstMediaUrl('highlight-video') }}" type="video/mp4">
                       Your browser does not support the video tag.
                   </video> --}}
                    <iframe id="movie_player" style="width: 100%; height: 350px;"  src="{{ $mentor->mentor_video_url }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    <div class="mt-3">
                        <div class="summary-desc row">
                            <div class="col-xs-12 col-sm-4 mb-4">
                              <div class="summary-desc__item">
                                  <img src="{{URL::asset('/img/icon/video-player-gold.svg')}}" alt="tatler-lesson">
                                  <div class="summary-desc__item__text">
                                      <h2>{{ $mentor->getTotalDuration() }} Menit</h2>
                                      <span class="body-2">Total Durasi</span>
                                  </div>
                              </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 mb-4">
                              <div class="summary-desc__item">
                                  <img src="{{URL::asset('/img/icon/education-gold.svg')}}" alt="tatler-class">
                                  <div class="summary-desc__item__text">
                                      <h2>{{ count($lessons) }} Pelajaran</h2>
                                      <span class="body-2">Dari Guru</span>
                                  </div>
                              </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 mb-4">
                              <div class="summary-desc__item">
                                  <img src="{{URL::asset('/img/icon/user.svg')}}" alt="tatler-class">
                                  <div class="summary-desc__item__text">
                                      <h2>{{ $totalTransaction }} Siswa</h2>
                                      <span class="body-2">Sudah Membeli</span>
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mentor-show__video-desc col-lg-5 col-md-12">
                    <label class="overline">{{ $mentor->profesi }}</label>
                    <h1>{{ $mentor->user->name }}</h1>
                    @if($mentor->getTotalStudent() > 0)
                        <span>{{ $mentor->getTotalStudent() }} orang sudah membeli</span>
                    @endif
                    <div class="mentor-show__price">
                        <span>{{ 'Rp '.number_format($mentor->price,2,',','.') }}</span>
                    </div>
                    <div class="position-absolute d-flex" style="bottom: -35px;right: 0px">
                    </div>
                    @empty(Auth::check() && Auth::user()->hasMentor($mentor->mentor_id))
                    <div class="d-flex">
                        <form action="{{ route('wishlists.store') }}" method="post">
                            @csrf
                            <input type="hidden" name="wishable_id" value="{{$mentor->mentor_id}}">
                            <input type="hidden" name="wishable_type" value="{{ \App\Wishlist::TYPE_MENTOR }}">
                            <button class="btn-outline--large"><i class="fas fa-heart"></i></button>
                        </form>
                        <form action="{{ route('carts.store') }}" method="POST" class="ml-2">
                            @csrf
                            <input type="hidden" name="cartable_id" value="{{$mentor->mentor_id}}">
                            <input type="hidden" name="cartable_type" value="{{\App\Wishlist::TYPE_MENTOR}}">
                            <button class="btn--large"><i class="fas fa-cart-plus"></i></button>
                        </form>
                    </div>
                    @else
                        <div class="d-flex">
                            <h4 class="text-white">Kamu sudah berlangganan kelas ini</h4>
                        </div>
                    @endempty
                </div>
            </div>
            <div class="row mt-5">
              <div class="col">
                <h1 class="mb-3">Tentang Mentor</h1>
                <p class="body-2">{{ $mentor->desc }}</p>
              </div>
            </div>
        </div>
    </div>
    
    <?php //$comment = $mentor->getBestRating() ?>
    {{--@if(!empty($comment))
        <div class="mentor__fetured-comments mb-5">
        <div class="container">
            <h1 class="mb-3">Featured Comment</h1>
            <div class="mentor__fetured-comments__comment">
                <div class="mentor__fetured-comments__avatar">
                    <img src="{{ $comment->getAvatar() ?: asset('/img/mentor-img/comment-avatar-dummy.png')}}" alt="Best Comment" style="max-height: 400px; max-width: 400px; object-fit: cover">
                </div>
                <div class="mentor__fetured-comments__text">
                    <h4 class="mb-1">{{ $comment->user->name }}</h4>
                    <div class="mentor__fetured-comments__text__rating mb-1">
                        @for ($i = 1; $i <= intval($comment->rating); $i++)
                            <img src="{{URL::asset('/img/icon/rating-icon.png')}}" alt="tatler-lesson">
                        @endfor
                    </div>
                    <span class="mentor__fetured-comments__text__time">
                    2 months ago
                </span>
                    <p class="body-2 mt-2">{{$comment->testimonial}}</p>
                </div>
            </div>
        </div>
    </div>
    @endif --}}


    @if(!empty(Auth::check() && Auth::user()->hasMentor($mentor->mentor_id)))
        @include('../partials/landing-page/rating')
    @endempty
    @isset($bestRating)
    @php $ratingAuthor = \App\User::find($bestRating->user_id); @endphp
      <div class="mentor__fetured-comments mb-5">
          <div class="container">
              <h1 class="mb-3">Penilaian Terpilih</h1>
              <div class="mentor__fetured-comments__comment">
                  <div class="mentor__fetured-comments__text">
                      <h4 class="mb-1">{{ $ratingAuthor->name ?? '' }}</h4>
                      <div class="mentor__fetured-comments__text__rating mb-1">
                          @for ($i = 1; $i <= $bestRating->rating; $i++)
                              <img src="{{URL::asset('/img/icon/rating-icon.png')}}" alt="tatler-lesson">
                          @endfor
                      </div>
                      <span class="mentor__fetured-comments__text__time">
                      {{ Carbon\Carbon::parse($bestRating->created_at)->locale('id')->isoFormat('D MMMM YYYY') ?? '' }}
                  </span>
                      <p class="body-2 mt-2">{{ $bestRating->testimonial ?? '' }}</p>
                  </div>
              </div>
          </div>
      </div>
    @endisset

    <div class="lesson-plan mt-5">
        <div class="container">
            <h1 class="mb-3">Daftar Kelas</h1>
            <div class="lesson-plan__box">
                @foreach($lessons as $i => $lesson)
                    <a href="{{ route('main.mentors.lessons.show',[$mentor->user->username,$lesson->slug]) }}">
                        <div class="lesson-plan__box__list__wrapper">
                            <div class="lesson-plan__box__list position-relative">
                                <div class="lesson-plan__box__list__number">{{$i+1}}</div>
                                <div class="lesson-plan__box__list__text">
                                    <h5>{{ $lesson->title }}</h5>
                                    <p class="body-2">
                                        {{ $lesson->desc }}
                                    </p>
                                    @if(Auth::check() && Auth::user()->isFinishWatching($lesson->id))
                                        <h3 class="position-absolute" style="top: 10px; right: 10px; color: #FFAE04 !important;"><i class="fas fa-check"></i></h3>
                                    @endif
                                </div>
                            </div>
                            <div class="lesson-plan__box__list__divider"></div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="mentor__comment-form mt-5 mb-5 d-none">
        <div class="container" style="max-width: 400px">
            <div class="d-flex flex-column justify-center align-items-center">
                <p>Beri Pendapat</p>
                <div class="mb-3" style="font-size: 2em">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
                <textarea type="text" name="txtReview" placeholder="Tulis pendapatmu" class="header__input"></textarea>
                <button class="btn--large border-0" data-toggle="modal" data-target="#exampleModal">Kirim Review</button>
            </div>
        </div>
    </div>

    <div class="share-course mt-5">
        <div class="container">
            <h6>Share Course</h6>
            <div class="d-flex justify-center">
              <div class="share-course__icons">
                  <a href="#">
                      <div class="footer__socmed__icon"><i class="fab fa-twitter"></i></div>
                  </a>
                  <a href="#">
                      <div class="footer__socmed__icon"><i class="fab fa-facebook"></i></div>
                  </a>
                  <a href="#">
                      <div class="footer__socmed__icon"><i class="fab fa-linkedin"></i></div>
                  </a>
                  <a href="#">
                      <div class="footer__socmed__icon"><i class="fab fa-whatsapp"></i></div>
                  </a>
              </div>
            </div>
        </div>
    </div>

    <div class="container mb-5">
        <div class="video-collection">
            <h1>Rekomendasi Mentor untuk Anda</h1>
            {{-- <div class="row justify-content-md-center">
                <p class="body-2 col col-md-8 mb-5">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint.</p>
            </div> --}}
            <div class="row mb-4">
                @php
                    $mentorRecommendations = \App\Mentor::where('mentor_id','!=',$mentor->mentor_id)->take(3)->get();
                @endphp
                @foreach ($mentorRecommendations as $mentorRecommendation)
                <div class="col-md-4 video-collection__item mb-4">
                  <a href="{{ url('/mentors',$mentorRecommendation->user->username) }}">
                    <img class="mb-1" src="{{$mentorRecommendation->getFirstMediaUrl('primary-photo') ?: asset('/img/mentor-img/mentor-image-small.png')}}" alt="tatler-class-mentor-image" style="height: 220px; object-fit: cover">
                    <div class="body-3">{{ $mentorRecommendation->user->name }}</div>
                    <label class="label-1">{{ $mentorRecommendation->profesi }}</label>
                  </a>
                </div>
                @endforeach
            </div>
            <div class="row justify-content-md-center justify-content-xs-start px-3">
                <a href="{{ url('/mentors') }}" class="btn--medium">Lihat Semua Mentor</a>
            </div>
        </div>
    </div>

    @guest
        @include('../partials/landing-page/try-trial')
    @endguest

    
@endsection

@section('script')
    <script >
        $(document).ready(function () {

            function addToCart(cartable_id, cartable_type) {
                $.ajax({
                    url: "{{ route('carts.store') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        cartable_id,
                        cartable_type
                    },
                    success(res) {
                        $('#cartCounter').html();
                        showToast(res.type, res.message);
                    },
                    error() {
                        showToast('error', 'Oops, Terjadi kesalahan. Silahkan coba beberapa saat lagi')
                    }
                });
            }
        })
    </script>
@endsection

