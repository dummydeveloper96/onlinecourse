@extends('layouts.app')

@section('content')

  <div class="privacy-detail">
    <div class="container">
      <div class="page-title">
        <h1>{{ $privacy->title }}</h1>
      </div>
    </div>
  </div>
  <div class="container news-detail d-flex flex-column">
    <div class="author">
      {{ ucwords($privacy->user->name) }}
    </div>
    <div class="upload-date">
      {{ Carbon\Carbon::parse($privacy->created_at)->format('d M Y') }}
    </div>
    <div class="body secondary-font">
      {!! $privacy->content !!}
    </div>
  </div>

@endsection
