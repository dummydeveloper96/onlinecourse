@extends('layouts.app')

@section('style')
  <style>
    .square {
      position: relative;
      width: 100%;
      max-width: 200px;
    }

    .square:after {
      content: "";
      display: block;
      padding-bottom: 100%;
    }

    .content {
      position: absolute;
      width: 100%;
      background: #fff;
      height: 100%;
    }
  </style>
@endsection
@section('content')
  <div class="profile">
    {{-- Jumbotron --}}
    <div class="row jumbotron banner">
      {{-- <a href="https://www.freepik.com/free-photos-vectors/background">Background photo created by kjpargeter - www.freepik.com</a> --}}
      {{-- Avatar --}}
      <div class="container">
        <div class="row">
          @if(!empty($user->getFirstMediaUrl('avatar-photo')))
          <div class="col-lg-3 col-md-3 col-sm-12 avatar d-flex justify-content-center align-items-center">
            <img src="{{ $user->getFirstMediaUrl('avatar-photo') }} " alt="avatar" class="img-fluid">
          </div>
          @endif
          <div class="col-lg-9 col-md-3 col-sm-12">
            <div class="d-flex flex-column">
              <div>
                <h1>{{ $user->name }}</h1>
              </div>
              <div>
                <h6>{{ $user->email }}</h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div id="tabs" class="d-flex flex-column">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Profil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="order-tab" data-toggle="tab" href="#order" role="tab" aria-controls="order" aria-selected="false">Riwayat Belanja</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="change-password-tab" data-toggle="tab" href="#changepassword" role="tab" aria-controls="changepassword" aria-selected="false">Ubah Password</a>
          </li>
        </ul>
        <div class="tab-content mt-3">
          <div class="tab-pane active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="d-flex flex-column">
              <div class="d-flex flex-row-reverse mb-3">
                <button id="editProfile" class="btn btn-edit-profile d-flex align-center" data-toggle="modal" data-target=".bd-example-modal-lg">
                  <ion-icon style="font-size: 22px;" name="create-outline"></ion-icon>&nbsp;&nbsp;<span>Ubah Profil</span>
                </button>
              </div>
              <div class="col-sm-12 mb-3">
                <label class="profile-label" for="Nama">Nama</label>
                <h6>{{ $user->name }}</h6>
              </div>
              <div class="col-sm-12 mb-3">
                <label class="profile-label" for="Email">Email</label>
                <h6>{{ $user->email }}</h6>
              </div>
              <div class="col-sm-12 mb-3">
                <label class="profile-label" for="Username">Username</label>
                <h6>{{ $user->username }}</h6>
              </div>
              <div class="col-sm-12 mb-3">
                <label class="profile-label" for="Gender">Jenis Kelamin</label>
                <h6>{{ $user->txt_gender }}</h6>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="order" role="tabpanel" aria-labelledby="order-tab">
            @php $invoiceNo = ''; @endphp
            @forelse($user->transactions as $order)
              <div class="d-flex flex-column transaction_item mb-3">
                <div class="d-flex flex-row justify-content-between transaction_header">
                  <h2 class="transaction_code">{{ $order->transaction_cd }} <span class="secondary-font {{ \App\Transaction::$badgeClass[$order->payment_status] }}">{{ \App\Transaction::$paymentStatus[$order->payment_status] }}</span></h2>
                  <h3 class="transaction_date">{{ Carbon\Carbon::parse($order->created_at)->format('d M Y') }}</h3>
                </div>
                @foreach($order->details as $details)

                <div class="row">
                  <div class="col cart__items">
                    <div class="cart__items__item mb-4">
                      <div class="cart__items__item__img">
                        <img src="{{asset($details->transactionable->getFirstMediaUrl('primary-photo'))}}" alt="">
                      </div>
                      <div class="cart__items__item__details">
                        <div class="cart__items__item__details__btm">
                          <div class="cart__items__item__details__btm__right">
                            <div class="cart__items__item__details__price">
                              <a href="{{ url('/mentors',$details->transactionable->user->username) }}">{{ $details->transactionable->user->name }}</a>
                              <div class="mentor_price">{{ 'Rp '.number_format($details->transactionable->price,2,',','.') }}</div>
                            </div>
                          </div>
                          <div class="transaction__mentor_summary">
                            <div class="body-2 font-weight-bold">{{ $details->transactionable->profesi }}</div>
                            <p class="body-2">{{ $details->transactionable->desc }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach
                @if($order->payment_status == \App\Transaction::PAYMENT_STATUS_PENDING && !empty($order->pdf_url))
                  <div class="transaction-how-to">
                    <a id="howToPay" href="{{ $order->pdf_url }}" class="btn btn-link" target="_blank"><i class="fas fa-exclamation-circle mr-2"></i>Tata cara pembayaran</a>
                  </div>
                @endif
                <div class="transaction_total_price">
                  Total pembayaran:&nbsp;{{ 'Rp '.number_format($order->ttl_prc_net,2,',','.') }}
                </div>
              </div>
            @empty
              <h4 class="text-white">Belum ada transaksi</h4>
            @endforelse
          </div>
          <div class="tab-pane" id="changepassword" role="tabpanel" aria-labelledby="change-password-tab">
            <div class="d-flex flex-column">
              <form action="{{ route('main.user.profile.update-password') }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                  <label for="old">Password Lama</label>
                  <input type="password" name="password" id="oldPassword" class="form-control w-50">
                </div>
                <div class="form-group">
                  <label for="new">Password Baru</label>
                  <input type="password" name="new_password" id="newPassword" class="form-control w-50">
                </div>
                <div class="form-group">
                  <label for="confirm">Ulangi Password Baru</label>
                  <input type="password" name="new_password_confirmation" id="confirmPassword" class="form-control w-50">
                </div>
                <button type="submit" class="btn btn--medium">Simpan</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="editProfileModal" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Ubah Profil</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="{{ route('main.user.profile.update',Auth::id()) }}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="name" class="col-form-label">Nama</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}">
              </div>
              <div class="form-group">
                <label for="Gender" class="col-form-label">Jenis Kelamin</label>
                <select name="gender" class="form-control select2bs4 " style="width: 100%;" >
                  @php $genders = \App\Helper::$lblGander @endphp
                  @foreach ($genders as $key => $val)
                    @if (old('gender',$user->gender) == $key)
                      <option value="{{ $key }}" selected>{{ $val }}</option>
                    @else
                      <option value="{{ $key }}">{{ $val }}</option>
                    @endif
                  @endforeach
                </select>
                @error('gender')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
              </div>

              <div class="form-group">
                <label for="inpAvatarPhoto">Avatar Photo <small>(.jpg, .jpeg, .png)</small></label>
                <div class="custom-file">
                  <input id="inpAvatarPhoto" value="{{ old('avatar_photo') }}" name="avatar_photo" type="file" class="custom-file-input @error('avatar_photo') is-invalid @enderror" accept="image/*"  >
                  <label class="custom-file-label" for="inpAvatarPhoto">Choose file...</label>
                  @error('avatar_photo')
                  <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                  @enderror
                </div>
              </div>

              <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script>
    $(document).ready(function () {
      $('#editProfile').click(function () {
        
      })
      var url = document.location.toString();
      if (url.match('#')) {
        $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
      }

      // Change hash for page-reload
      $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
      })
      @isset($swal)
        Swal.fire({
          icon: '{{ $swal['type'] }}',
          title: '{{ $swal['text'] }}',
          showConfirmButton: false,
          timer: 2000
        })
      @endisset
      $('#inpAvatarPhoto').on('change',function(e){
          $(this).next('.custom-file-label').html(e.target.files[0].name);
      })
    })

  </script>
@endsection