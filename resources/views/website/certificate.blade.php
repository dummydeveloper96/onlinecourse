@extends('layouts.certificate-layout')
@section('content')
<div class="outer-background">
  <div class="d-flex border border-secondary h-100">
    <div class="w-100 d-flex justify-content-center align-items-center flex-column border-inner text-center p-3">
      <div class="logo mb-3">
        <img src="{{URL::asset('/img/dummy-logo.png')}}">
      </div>
      <div class="big-title mb-5">
        <b>
          Certificate of Completion
        </b>
      </div>
      <p class="cert-text">This certificate is awarded to:</p>
      <div class="border-bottom mb-3 w-50">
        <p id="name" class="cert-name">Muhammad Shafrizal</p>
      </div>
      <p class="cert-text mb-5">For completing 'Nama Kursus' on 'Tanggal Kursus Selesai'</p>
      <div class="d-flex flex-column text-center justify-content-center">
        <p class="mentor-signature">Bobbi Brown</p>
        <p class="mentor-name">Bobbi Brown</p>
      </div>
    </div>
  </div>
</div>
<div class="action">
  <a href="#" class="btn btn-primary">Download</a>
  <a href="#" class="btn btn-secondary">Print</a>
</div>
@endsection
@section('style')
<style>
.outer-background {
  width: 800px;
  min-height: 600px;
  padding: 20px;
}
.border {
  border-width: 3px;
}
.border-inner {
  border: 1px solid #333;
}
.big-title {
  font-size: 34px;
  font-style: italic
}
.cert-text {
  font-size: 18px;
  color: #333;
}
p.cert-name {
  font-weight: bold;
  font-size: 28px;
  margin: 0;
}
.mentor-signature {
  font-size: 24px;
  font-family: 'Great Vibes';
  text-decoration: underline;
  margin: 0;
}
.mentor-name {
  font-size: 12px;
  margin: 0;
}
.btn-primary {
  background-color: #FE8C07;
  color: #fff;
  border: none;
}
.btn-primary:hover {
  background-color: #D77400;
  border: none;
}
</style>
@endsection