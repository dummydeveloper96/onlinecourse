@extends('layouts.app')

@section('content')
<div class="quiz-wrapper">
  <div class="container d-flex flex-column">
    <h1>Quiz</h1>
    <div class="d-flex flex-row flex-wrap mb-3">
      <b>
      Nama Kursus
      |
      Pelajaran Ke-N / Melukis Abstrak
      </b>
    </div>
    <form action="" method="post">
      @csrf
      @for ($i = 0; $i < 5; $i++)
        <div class="form-group mb-5">
          <label for="Question" class="form-control-label text-light font-weight-bold mb-2">{{ ($i + 1) }}.&nbsp;Apa nama peliharan Hanif?</label>
          <div class="form-check mb-3">
            <input class="form-check-input" type="radio" name="exampleRadios{{$i}}" id="exampleRadios{{($i+1)}}" value="option1" checked>
            <label class="form-check-label" for="exampleRadios{{($i+1)}}" class="text-light">
              Ananta
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="radio" name="exampleRadios{{$i}}" id="exampleRadios{{($i+1)}}" value="option2">
            <label class="form-check-label" for="exampleRadios{{($i+1)}}" class="text-light">
              Joni
            </label>
          </div>
          <div class="form-check mb-3">
            <input class="form-check-input" type="radio" name="exampleRadios{{$i}}" id="exampleRadios{{($i+1)}}" value="option3">
            <label class="form-check-label" for="exampleRadios{{($i+1)}}" class="text-light">
              Destroyer in The Jungle
            </label>
          </div>
        </div>
      @endfor
      <button type="submit" class="btn btn--medium">
        Jawab
      </button>
      <a href="#" class="btn btn-outline--medium">Kembali</a>
      {{-- Remove this button if finished integrating with backend --}}
      <p class="text-light"> Remove show modal button after integrating with backend</p>
      <button type="button" id="editProfile" class="btn btn-edit-profile d-flex align-center" data-toggle="modal" data-target=".bd-example-modal-lg">
        <ion-icon style="font-size: 22px;" name="create-outline"></ion-icon>&nbsp;&nbsp;<span>Show Modal</span>
      </button>
    </form>
  </div>
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="editProfileModal" aria-hidden="false">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Skor</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        {{-- Add class score-good if score > N or add score-bad if score < N --}}
        <div class="modal-body">
          <div class="d-flex justify-content-center align-items-center flex-column">
            <div class="score-number-wrapper d-flex justify-content-center align-items-center score-good text-center mb-3">
              <div class="score-number">
                80
              </div>
            </div>
            <div class="score-text">
              Selamat! Anda mendapat nilai score_number!
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('style')
<style>
  label {
    color: #fff
  }
  .score-number {
    font-size: 48px;
    font-weight: bold;
    color: #fff;
  }
  .score-number-wrapper {
    background-color: #38A700;
    width: 100px;
    height: 100px;
    border-radius: 50%;
  }
  .score-text {
    font-weight: bold;
    font-size: 18px;
  }
</style>
@endsection