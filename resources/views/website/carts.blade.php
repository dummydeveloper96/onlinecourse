@extends('layouts.app')

@section('content')

  <div class="cart container">
    <h1>Keranjang</h1>
    @if(count($cart['items']) > 0)
      <p class="body-2 mb-4">Daftar kelas di keranjang anda.</p>
      <span class="cart__counter color__orange_primary">Total kelas:&nbsp;<span id="itemCounter">{{ count($cart['items']) }}</span></span>
      <div class="row">
        <div class="col-md-8 cart__items">
          @foreach ($cart['items'] as $item)
            <div class="cart__items__item mb-4" id="cart{{$item->id}}">
              <div class="cart__items__item__img">
                <img src="{{$item->attributes->img_url}}" alt="Mentor Image">
              </div>
              <div class="cart__items__item__details">
                <div class="cart__items__item__details__top">
                  <h2 class="text-truncate">{{ $item->name }} </h2>
                </div>
                <div class="cart__items__item__details__btm">
                  <div class="cart__items__item__details__btm__right">
                    <div class="cart__items__item__details__price">{{ \App\Helper::IDR($item->price) }}</div>
                  </div>
                  <div class="cart__items__item__details__btm__left flex-row">
                    <button class="btn btn-link btnRemoveItem" data-id="{{$item->id}}" style="color: #FFAE04;"><i class="fas fa-trash"></i></button>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <div class="col-md-4">
          <div class="cart__total">
            <div class="cart__total__title color__black-primary">Ringkasan belanja</div>
            <div class="cart__total__divider"></div>
            <div class="cart__total__price mb-2">
              <div class="cart__total__price__left color__black-secondary">Total Harga</div>
              <div class="cart__total__price__right color__black-primary" id="txtTotal" data-amount="{{$cart['total']}}">{{ \App\Helper::IDR($cart['total']) }}</div>
            </div>
            <form id="payment-form" method="post" action="{{ route('main.midtrans.finish') }}">
              @csrf
              <input type="hidden" name="result_type" id="result-type" value="">
              <input type="hidden" name="result_data" id="result-data" value="">
            </form>
            <form action="{{ route('cart.checkout') }}" method="POST" class="d-none" id="formCheckout">
              @csrf
              <button class="btn btn-primary btn-block">Check Out</button>
            </form>
            <button id="pay-button1" class="btn btn-primary btn-block">Check Out</button>
            <a class="btn btn-primary" href="{{ url('/mentors') }}">Kembali Belanja</a>
          </div>
        </div>
      </div>
    @else
      <p class="body-2 mb-4">Tidak ada kelas dialam keranjang</p>
    @endif
  </div>

@endsection

@section('script')
  <script src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}"
    data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
  <script>
    var _destroyUrl = '{{route('carts.destroy',"__ID__")}}';
      $(document).ready(function () {
        $('.btnRemoveItem').click(function () {
          var jThis = $(this);
          var _id = jThis.data('id');
          var _url = _destroyUrl.replace('__ID__',_id)
          $.ajax({
            url : _url,
            type : 'POST',
            data : {
              _token: "{{ csrf_token() }}",
              _method: "DELETE"
            },
            success(res){
              showToast(res.type, res.message);
              $('#txtTotal').data('amount',res.data.amount);
              $('#txtTotal').html(res.data.total);
              itemCOunt = $('#itemCounter').text();
              $('#itemCounter').text(itemCOunt > 1 ? itemCOunt - 1 : 'tidak ada');
              $('#cart'+_id).remove();
            }
          })
        })
      })
  </script>
  <script type="text/javascript">

    $(document).ready(function () {
      $('#pay-button1').click(function (event) {
        event.preventDefault();
        var jThis = $(this);
        jThis.attr("disabled", "disabled");
        var amount = $("#txtTotal").data('amount');
        if (amount == 0){
          $("#formCheckout").submit();
          jThis.prop("disabled", false);
          return false;
        } else {
          $.ajax({
            url: '{{ route('main.midtrans.snap') }}',
            cache: false,
            success: function(data) {
              //location = data;
              console.log('token = '+data);

              var resultType = document.getElementById('result-type');
              var resultData = document.getElementById('result-data');
              snap.pay(data, {

                onSuccess: function(result){
                  changeResult('success', result);
                  console.log(result.status_message);
                  console.log(result);
                  $("#payment-form").submit();
                },
                onPending: function(result){
                  changeResult('pending', result);
                  console.log(result.status_message);
                  $("#payment-form").submit();
                },
                onError: function(result){
                  changeResult('error', result);
                  console.log(result.status_message);
                  $("#payment-form").submit();
                }
              });
            },
            complete : function () {
              jThis.prop("disabled", false);
            }
          });
        }
      });
    });

    function changeResult(type,data){
      $("#result-type").val(type);
      $("#result-data").val(JSON.stringify(data));
      //resultType.innerHTML = type;
      //resultData.innerHTML = JSON.stringify(data);
    }
  </script>
@endsection