@extends('layouts.app')
@section('content')
  <div class="news mb-5">
    <img src="{{URL::asset('/img/decoration/header-decoration.png')}}" class="header-decoration">
    <div class="container">
      <div class="banner-article">
        <h1>Kumpulan Artikel</h1>
      </div>
    </div>
  </div>
  @if (!empty($errors->first()))
  <div class="container">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{ $errors->first() }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
  </div>
  @endif
  <div class="container news-list">
    {{-- Search --}}
    <div class="row">
      <div class="col-xs-12 col-md-3 mb-4">
        <div class="d-flex flex-column">
            <form action="{{ route('news.search') }}" method="post">
              @csrf
                <div class="input-group">
                  <input type="text" name="search" id="searcArticle" class="form-control" placeholder="Cari artikel...">
                  <div class="input-group-append">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                  </div>
                </div>
            </form>
        </div>
      </div>
      {{-- News Column --}}
      <div class="col-xs-12 col-md-9 mb-4">
        <div class="d-flex flex-column">
          @forelse ($news as $item)
              <div class="card news-item mb-3">
                <div class="card-body">
                    <div class="news-thumbnail-wrapper">
                      <img class="news-thumbnail img-fluid" src="{{ $item->getThumbnail() ?: 'https://fakeimg.pl/300x300/FFAE04/FFFFFF?text='. Carbon\Carbon::parse($item->created_at)->format('d M Y') .'&font=bebas&font_size=40' }}" alt="{{ $item->title }}">
                    </div>
                    <div class="news-text-wrapper">
                      <div class="news-text">
                        <p class="card-title color__black_primary">{{ $item->title }}</p>
                        <p class="card-subtitle color__black_secondary">{{ Carbon\Carbon::parse($item->created_at)->format('d M Y') }}</p>
                        <p class="news-summary color__black_secondary">{{ $item->news_summary }}</p>
                        <a href="{{ route('readNewsFrontend', $item->id) }}" class="btn--medium">Baca</a>
                      </div>
                    </div>
                </div>
              </div>
          @empty
            <div class="text-center">
              @if (isset($result))
                  <h3 class="text-white">Pencarian tidak ditemukan</h3>
              @else
                  <h3 class="text-white">Belum ada artikel</h3>
              @endif
            </div>
          @endforelse
          
          {{ $news->links() }}
        </div>
      </div>
    </div>
  </div>
@endsection
@section('script')
  <script>
    $(document).ready(function () {
      $('.alert').alert();
    })
  </script>
@endsection