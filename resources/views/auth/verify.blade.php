@extends('layouts.app')

@section('content')
    <div class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-card">
                        <div class="color__black_primary">{{ __('Verify Your Email Address') }}</div>
                        {{ __('Sebelum melanjutkan, anda harus verifikasi email Anda terlebih dahulu.') }}
                        {{ __('Mohon tunggu tautan verifikasi sampai dengan 5 menit.') }},
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0 align-baseline text-major">{{ __('Klik disini untuk verifikasi.') }}</button>.
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
