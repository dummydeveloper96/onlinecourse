<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('question_answer')) {
            Schema::create('question_answer', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('quiz_questions_id')->unsigned();
                $table->string('answer');
                $table->string('choice_word');
                $table->boolean('is_answer');
                $table->smallInteger('status');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_answer');
    }
}
