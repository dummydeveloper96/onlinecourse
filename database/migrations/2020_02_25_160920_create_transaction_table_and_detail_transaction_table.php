<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTableAndDetailTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_cd')->unique();
            $table->unsignedBigInteger('student_id');
            $table->decimal('ttl_prc_discount',17,2);
            $table->decimal('ttl_prc_tax',17,2);
            $table->decimal('ttl_prc_net',17,2);
            $table->smallInteger('payment_status');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('student_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        Schema::create('transaction_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id');
            $table->morphs('transactionable','transactionable_type_id'); // Mentor or Lesson
            $table->smallInteger('days_duration'); // per days | ga tau dibutuhin / ga
            $table->decimal('price',17,2);
            $table->timestamps();
            $table->foreign('transaction_id')
                ->references('id')
                ->on('transactions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
        Schema::dropIfExists('transaction_detail');
    }
}
