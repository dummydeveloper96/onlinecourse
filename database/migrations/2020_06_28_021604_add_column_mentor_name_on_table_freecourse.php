<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnMentorNameOnTableFreecourse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('freecourses', function (Blueprint $table) {
            $table->string('mentor_name');
            $table->dropForeign('freecourses_mentor_id_foreign');
            $table->dropColumn(['mentor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('freecourses', function (Blueprint $table) {
            $table->dropColumn(['mentor_name']);
            $table->unsignedBigInteger('mentor_id');

            $table->foreign('mentor_id')
                ->references('mentor_id')
                ->on('mentors')
                ->onDelete('cascade');
        });
    }
}
