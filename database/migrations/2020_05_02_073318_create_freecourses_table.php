<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFreecoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freecourses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mentor_id');
            $table->unsignedBigInteger('thumbnail_photo')->nullable();
            $table->string('title');
            $table->text('desc');
            $table->text('video_url')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('mentor_id')
                ->references('mentor_id')
                ->on('mentors')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freecourses');
    }
}
