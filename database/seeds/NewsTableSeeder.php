<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Faker\Factory as Faker;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create('id_ID');
      for ($i=0; $i < 10; $i++) { 
        $text = $faker->sentence();
        $slugText = str_replace(' ', '-', $text);
        DB::table('news')->insert([
          'title' => $text,
          'slug' => $slugText,
          'desc' => $faker->text(),
          'author_id' => 4,
          'created_at' => $faker->dateTime()
        ]);
      }
    }
}
