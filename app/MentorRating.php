<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class MentorRating extends Model
{
    public $incrementing = false;
    protected $primaryKey = ['mentor_id', 'user_id'];
    protected $with = ['user'];
    protected $fillable = ['rating', 'testimonial', 'is_show', 'created_at', 'updated_at'];


    public static $createRules = [
        'rating' => 'required|min:1|exists:users,id',
        'testimonial' => 'required|max:500',
        'is_show' => 'required',
    ];

    //---> Illegal offset type while updating model
    //---> because primary key more than 1 --> add this
    //https://laracasts.com/discuss/channels/laravel/illegal-offset-type-while-updating-model?page=1
    protected function setKeysForSaveQuery(Builder $query)
    {
        return $query->where('mentor_id', $this->getAttribute('mentor_id'))
            ->where('user_id', $this->getAttribute('user_id'));
    }

    public function getAvatar(){
        $user = User::find($this->user_id);
        return $user->getFirstMediaUrl('avatar-photo');
    }
    public function user(){
        return $this->belongsTo('App\User','mentor_id');
    }
}
