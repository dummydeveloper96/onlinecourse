<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Freecourse extends Model implements hasMedia
{
    use HasMediaTrait;
    use SoftDeletes;
    const FREECOURSE_MEDIA_THUMBNAIL = 'thumbnail-photo';

    public static function rules($scenario = 'create',$id = false,$merge=[]){
        $rules = [
            'create' => [
                'title'             => 'required',
                'mentor_name'       => 'required',
                'desc'              => 'required',
                'thumbnail_photo'   => 'image',
                'video_url'         => 'required',
            ],
            'update' => [
                'title'             => 'required',
                'mentor_name'       => 'required',
                'desc'              => 'required',
                'thumbnail_photo'   => 'image',
                'video_url'         => 'required',
            ]
        ];
        return array_merge($rules[$scenario],$merge);
    }

    public function mentor() {
        return $this->belongsTo(Mentor::class,'mentor_id');
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection(self::FREECOURSE_MEDIA_THUMBNAIL)->singleFile();
    }
}
