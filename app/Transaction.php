<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Transaction extends Model
{
    public static $paymentStatus = [
        50  => 'pending',
        100 => 'capture',
        150 => 'refund',
        160 => 'partial_refund',
        170 => 'chargeback',
        180 => 'partial_chargeback',
        201 => 'authorize',
        401 => 'cancel',
        406 => 'deny',
        420 => 'failure',
        419 => 'expire',
        600 => 'settlement',
    ];

    public static $badgeClass = [
      50 => 'badge badge-pending',
      100 => 'badge badge-capture',
      150 => 'badge badge-refund',
      160 => 'badge badge-partial-refund',
      170 => 'badge badge-chargeback',
      180 => 'badge badge-partial-chargeback',
      201 => 'badge badge-authorize',
      401 => 'badge badge-cancel',
      406 => 'badge badge-deny',
      420 => 'badge badge-failure',
      419 => 'badge badge-expire',
      600 => 'badge badge-settlement',
    ];
    const PAYMENT_STATUS_PENDING = 50 ;
    const PAYMENT_STATUS_CAPTURE = 100;
    const PAYMENT_STATUS_REFUND = 150;
    const PAYMENT_STATUS_PARTIAL_REFUND = 160;
    const PAYMENT_STATUS_CHARGEBACK = 170;
    const PAYMENT_STATUS_PARTIAL_CHARGEBACK = 180;
    const PAYMENT_STATUS_AUTHORIZE = 201;
    const PAYMENT_STATUS_CANCEL = 401;
    const PAYMENT_STATUS_DENY = 406;
    const PAYMENT_STATUS_FAILURE = 420;
    const PAYMENT_STATUS_EXPIRE = 419;
    const PAYMENT_STATUS_SETTLEMENT = 600;
    protected $with = ['student','details'];

    protected $fillable = [
        'transaction_cd',
        'student_id',
        'ttl_prc_discount',
        'ttl_prc_tax',
        'ttl_prc_net',
    ];

    /**
     * Set status to Pending
     *
     * @return void
     */
    public function setPending()
    {
        $this->attributes['payment_status'] = self::PAYMENT_STATUS_PENDING;
        self::save();
    }

    /**
     * Set status to Success
     *
     * @return void
     */
    public function setSuccess()
    {
        $this->attributes['payment_status'] = self::PAYMENT_STATUS_SETTLEMENT;
        self::save();

        foreach ($this->details as $item) {
            $mentor = Mentor::find($item->transactionable_id);
            $mentor->transaction_count = $mentor->transaction_count + 1;
            $mentor->save();
            $studentMentor = StudentMentor::where('student_id', $this->student_id)->where('mentor_id', $mentor->mentor_id)->first();
            if (empty($studentMentor)){
                $studentMentor = new StudentMentor;
            }
            $studentMentor->student_id = $this->student_id;
            $studentMentor->mentor_id = $mentor->mentor_id;
            $studentMentor->expired_dt = date('Y-m-d H:i:s',strtotime('now +20 years'));
            $studentMentor->save();

            foreach ($mentor->lessons as $key => $lesson) {
                $userLesson = UserLesson::where('user_id', $this->student_id)->where('lesson_id', $lesson->id)->first();
                if (empty($userLesson)){
                    $userLesson = new UserLesson;
                    $userLesson->user_id = $this->student_id;
                    $userLesson->lesson_id = $lesson->id;
                }
                $userLesson->buy_count += 1;
                $userLesson->expired_dt = date('Y-m-d H:i:s',strtotime('now +20 years'));
                $userLesson->is_done_watching = Helper::STATUS_NO;
                $userLesson->save();
            };
            $wishlist = Wishlist::where('wishable_id',$item->transactionable_id)
                ->where('wishable_type',$item->transactionable_type)
                ->where('user_id',$this->student_id)->first();
            if (!empty($wishlist))
                $wishlist->delete();

        }
    }

    /**
     * Set status to Failed
     *
     * @return void
     */
    public function setFailed()
    {
        $this->attributes['payment_status'] = self::PAYMENT_STATUS_FAILURE;
        self::save();
    }

    /**
     * Set status to Expired
     *
     * @return void
     */
    public function setExpired()
    {
        $this->attributes['payment_status'] = self::PAYMENT_STATUS_EXPIRE;
        self::save();
    }

    public function student()
    {
        return $this->belongsTo('App\User','student_id');
    }
    public function details()
    {
        return $this->hasMany(TransactionDetail::class);
    }
    public function getPaymentStatus (int $code) {
      return self::$paymentStatus[$code];
    }
}