<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentMentor extends Model
{
    protected $primaryKey = ['student_id', 'mentor_id'];
    public $incrementing = false;
}
