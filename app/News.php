<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class News extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $primaryKey = 'id';
    protected $fillable = ['author','title', 'desc', 'visit_count', 'slug', 'news_thumbnail', 'news_summary'];

    public static $createRules = [
        'title' => 'required|max:255',
        'slug' => 'required|max:255',
        'desc' => 'required',
        'news_thumbnail' => 'required|max',
        'news_summary' => 'required'
    ];

    public function user () {
      return $this->belongsTo('App\User', 'author_id');
    }

    public function registerMediaCollection () {
      $this
      ->addMediaCollection('news_thumbnail')
      ->singleFile();
    }

    public function getThumbnail () {
      return $this->getFirstMediaUrl('news_thumbnail');
    }
}
