<?php

namespace App;

use Carbon\Carbon;

class Helper
{
    const STATUS_NO = 0;
    const STATUS_YES = 1;
    public static $lblGander = [1 => 'laki-laki', 2 => 'perempuan'];

    public static function IDR($amount){
        return "Rp " . number_format($amount,0,',','.');
    }
    public static function formatDate($value,$from = null, $to = null){
        $from = empty($from) ? config('app.datetime_format') : $from;
        $to = empty($to) ? config('app.datetime_format_sql') : $to;
        return Carbon::createFromFormat($from, $value)->format($to);
    }

    public static function youtubeURLtoEmbedURL($url){
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
        $longUrlRegex = '/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/';

        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }

        if (isset($youtube_id) || !empty($youtube_id)){
            $fullEmbedUrl = 'https://www.youtube.com/embed/' . $youtube_id ;
            return $fullEmbedUrl;
        } else{
            return false;
        }

    }
}