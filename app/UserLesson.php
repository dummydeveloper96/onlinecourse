<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasCompositePrimaryKey;
class UserLesson extends Model
{
    use HasCompositePrimaryKey;
    public $incrementing = false;
    protected $table = 'user_lessons';
    protected $primaryKey = ['user_id','lesson_id'];
    protected $fillable = ['user_id','lesson_id','last_watch_tm','watch_count', 'buy_count','expired_dt','created_at','updated_at'];
    protected $dates = ['expired_dt','created_at','updated_at'];
}
