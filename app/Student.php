<?php


namespace App;

class Student extends User
{
    protected function hasMentor($mentor_id){
        $this->transactions->details
            ->where('transactionable_type',Mentor::class)
            ->where('transactionable_id',$mentor_id)->first();
    }
}