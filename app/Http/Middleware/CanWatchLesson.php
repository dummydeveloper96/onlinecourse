<?php

namespace App\Http\Middleware;

use App\Lesson;
use App\Mentor;
use App\UserLesson;
use Closure;
use Illuminate\Support\Facades\Auth;

class CanWatchLesson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $reqParams = ($request->route()->parameters());
        $user = Auth::user();
        if ($user->hasRole(['student'])){
            if ($user->isTrial())
                return $next($request);

//            $lesson = Lesson::where('slug',$request->route()->parameters()['lesson'])->first();
//            $userHasLesson = UserLesson::where('user_id', Auth::id())->where('lesson_id', $lesson->id)->first();
            $mentor = Mentor::getMentorByUsername($request->route()->parameters()['mentor']);
//            if (!empty($userHasLesson))
            if (!empty(Auth::check() && Auth::user()->hasMentor($mentor->mentor_id)))
                return $next($request);
            else
                return redirect(url('/mentors',$reqParams['mentor']))
                    ->with('message',[
                        'status' => 403,
                        'type' => 'error',
                        'message' => 'Sebelum menonton Anda harus membeli kelas ini terlebih terlebih dahulu!']);

        }
        else if ($user->hasRole(['super-admin','admin', 'mentor','staff'])){
            return $next($request);
        }

        abort(403, 'Unauthorized action.');
    }
    
}
