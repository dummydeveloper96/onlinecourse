<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LessonQuiz;


class LessonQuizController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->LessonQuiz = new LessonQuiz();
            return $next($request);
        });
    }

    public function create(Request $request)
    {
        $this->LessonQuiz->setParams($request->input());
        $data = $this->LessonQuiz->create();

        return response()->json($data, $data['code']);
    }

    public function update(Request $request)
    {
        $this->LessonQuiz->setParams($request->input());
        $data = $this->LessonQuiz->update();

        return response()->json($data, $data['code']);
    }

    public function getDetailsQuizByID(Request $request, $quiz_id)
    {
        $this->LessonQuiz->setParams($request->input());
        $data = $this->LessonQuiz->getDetailsQuizByID($quiz_id);

        return response()->json($data, $data['code']);
    }

    public function getDetailsQuizByLessonID(Request $request, $lesson_id)
    {
        $this->LessonQuiz->setParams($request->input());
        $data = $this->LessonQuiz->getDetailsQuizByLessonID($lesson_id);

        return response()->json($data, $data['code']);
    }

    public function getQuizListByMentorID(Request $request, $mentor_id)
    {
        $this->LessonQuiz->setParams($request->input());
        $data = $this->LessonQuiz->getQuizListByMentorID($mentor_id);

        return response()->json($data, $data['code']);
    }
}
