<?php

namespace App\Http\Controllers;

use App\Mentor;
use App\StudentMentor;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public $tabIndex = "";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        $user = User::findByUsername($username);
        if ($this->isAdminPage()) {
            return redirect()->route('mentors.show',$username) ;
        }else{
            if ($username == Auth::user()->username)
                return view('website.profile')->with('user',$user);
            if ($user->hasRole('mentor'))
                return redirect(url('/mentors',$username));

            abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate(User::$updateRules);
        $user = User::find($id);
        $user->name = $validatedData['name'];
        $user->gender = $validatedData['gender'];
        DB::transaction(function () use ($user, $validatedData){
            $user->save();

            if (isset($validatedData['avatar_photo']) && !empty($validatedData['avatar_photo'])){
                $user->clearMediaCollection('avatar-photo');
                $user->addMedia($validatedData['avatar_photo'])->toMediaCollection('avatar-photo');
            }

            /*
            $mentor->addMedia($validatedData['highlight_video'])
                ->toMediaCollection('highlight-video');
            */
        });
        return back()->with('message',['status' => 200,'type' => 'success', 'message' => "Berhasi memberbarui profil"]);

    }
    public function updatePassword(Request $request)
    {
        $this->tabIndex = 'changepassword';
        $request->validate([
            'password' => 'required',
            'new_password' => 'required|string|confirmed|min:6|different:password'
        ]);

        if (Hash::check($request->password, Auth::user()->password) == false)
        {
            return response(['message' => 'Unauthorized'], 401);
        }

        $user = Auth::user();
        $user->password = Hash::make($request->new_password);
        $user->save();

        return response([
            'message' => 'Your password has been updated successfully.'
        ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // all wishlist page

    public function indexAllWishlist () {
      return view('Website/allWishlist');
    }

    // all notification page

    public function indexAllNotification () {
        $unRead = auth()->user()->unreadNotifications;
        if (!empty($unRead))
            $unRead->markAsRead();
        return view('website/allNotification')
            ->with(['notifications' => auth()->user()->notifications]);
    }

    // all purchases page

    public function indexAllPurchases () {
      return view('Website/allPurchases');
    }

    public function myCourses(){
//        $courses = Auth::user()->myCourses();
        $myMentors = StudentMentor::where('student_id',Auth::id())->get();
        $mentors = [];
        foreach ($myMentors as $mentor){
            $mentor = Mentor::find($mentor->mentor_id);
            if (!empty($mentor)){
                $mentors[] = $mentor;
            }
        }
        return view('website.my-course', ['mentors' => $mentors]);
    }

    public function certificate () {
      return view('website.certificate');
    }
}
