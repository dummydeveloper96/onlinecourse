<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faq;
use App\SupportDesk;
class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = SupportDesk::where('topic', 'faq')->get();
        return view('website.faq', compact('faqs'));
        
    }

    public function detail (Request $request) {
      try {
        $faq = SupportDesk::find($request->id);
        return view('website.faq-detail', compact('faq'));
      } catch (\Exception $exception) {
        return redirect()->back()->withErrors('Terjadi kesalahan, mohon coba lagi!');
      }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request)) {
          $faq = new Faq;
          $faq->question = $request->question;
          $faq->answer = $request->answer;
          $faq->order = $request->order;
          $faq->save();
          $faqs = Faq::all();
          return view('admin.faq.list', compact('faqs'));
        } else {
          return back()->with('error','Failed to save data');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return view('admin.faq.list');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = Faq::where('id',$id)->get();
        return view('admin.faq.edit')->with('faq', $faq);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $faq = Faq::find($id);
      $faq->question = $request->question;
      $faq->answer = $request->answer;
      $faq->order = $request->order;
      $faq->save();
      $faqs = Faq::all();
      return view('admin.faq.list', compact('faqs'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq = Faq::find($id);
        $faq->delete();
        $faqs = Faq::all();
        return view('admin.faq.list', compact('faqs'));
    }
}
