<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Lesson;
use App\StudentMentor;
use App\TransactionDetail;
use App\User;
use App\UserLesson;
use App\Wishlist;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;
use App\Mentor;
use App\Transaction;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart = [];
        $items = \Cart::session(Auth::id())->getContent();
        $cart = [
            'items' => $items,
            'total' => \Cart::getTotal()
        ];
        return view('website.carts')->with('cart',$cart);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mentor = Mentor::find($request->cartable_id);
        $conditions = [];

        \Cart::session(Auth::id());
        if (empty(\Cart::get($request->cartable_id))){
            \Cart::add([
                'id' => $request->cartable_id,
                'name' => $mentor->user->name,
                'price' => $mentor->price,
                'quantity' => 1,
                'attributes' => [
                    'cartable_id' => $request->cartable_id,
                    'cartable_type' => $request->cartable_type,
                    'img_url' => $mentor->getFirstMediaUrl('primary-photo')
                ],
                'conditions' => $conditions
            ]);
            return redirect()->route('carts.index')->with('message',['status' => 200,'type' => 'success', 'message' => "Kursus <b>{$mentor->user->name}</b> berhasil ditambah ke keranjang"]);
        }
        else{
            return redirect()->back()->with('message',['status' => 406,'type' => 'error', 'message' => 'Kursus ini sudah berada didalam ke keranjang!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \Cart::session(Auth::id());
        $message = '';
        $item = \Cart::get($id);
        if($item->attributes['cartable_type'] == Mentor::class) {
            $mentor = Mentor::find($id);
            $message = "Kursus <b>{$mentor->user->name}</b> berhasil dihapus dari keranjang!";
        };
        \Cart::remove($id);
        $total = \Cart::getTotal();
        return response()->json([
            'data' => [
                'total' => Helper::IDR($total),
                'amount' => $total,
            ],
            'status' => 200,
            'type' => 'warning',
            'message' => $message]);
    }

    public function clearAll(){
        \Cart::session(Auth::id());
        \Cart::clear();
        \Cart::session(Auth::id())->clear();
        return redirect()->route('carts.index');
    }


    public function checkout(){
        \Cart::session(Auth::id());
        $items = \Cart::getContent();
        if ($items->count() > 0 && \Cart::getTotal() == 0){
            DB::beginTransaction();
            try{
                $counter = DB::table('transactions')->latest('id')->first();
                $counter = empty($counter)? $counter+1 : intval($counter->id)+1;
                $transaction = new Transaction;
                $transaction->transaction_cd = "INV-GS-".date('dmY').$counter;
                $transaction->student_id = Auth::id();
                $transaction->ttl_prc_discount = 0;
                $transaction->ttl_prc_tax = 0;
                $transaction->ttl_prc_net = \Cart::getTotal();
                $transaction->payment_status = Transaction::PAYMENT_STATUS_SETTLEMENT;
                $transaction->save();
                foreach ($items as $item){
                    $mentor = Mentor::find($item->id);
                    $mentor->transaction_count = $mentor->transaction_count + 1;
                    $mentor->save();
                    $transactionDetail = new TransactionDetail;
                    $transactionDetail->transaction_id = $transaction->id;
                    $transactionDetail->transactionable_type = Mentor::class;
                    $transactionDetail->transactionable_id = $item->attributes['cartable_id'];
                    $transactionDetail->days_duration += 1;
                    $transactionDetail->price = $item->price;
                    $transactionDetail->save();


                    $studentMentor = StudentMentor::where('student_id', Auth::id())->where('mentor_id', $mentor->mentor_id)->first();
                    if (empty($studentMentor)){
                        $studentMentor = new StudentMentor;
                        $studentMentor->student_id = Auth::id();
                        $studentMentor->mentor_id = $mentor->mentor_id;
                    }
                    $studentMentor->expired_dt = date('Y-m-d H:i:s',strtotime('now +20 years'));
                    $studentMentor->save();

                    foreach ($mentor->lessons as $key => $lesson) {
                        $userLesson = UserLesson::where('user_id', Auth::id())->where('lesson_id', $lesson->id)->first();
                        if (empty($userLesson)){
                            $userLesson = new UserLesson;
                            $userLesson->user_id = Auth::id();
                            $userLesson->lesson_id = $lesson->id;
                        }
                        $userLesson->buy_count += 1;
                        $userLesson->expired_dt = date('Y-m-d H:i:s',strtotime('now +20 years'));
                        $userLesson->is_done_watching = Helper::STATUS_NO;
                        $userLesson->save();
                    };
                    $wishlist = Wishlist::where('wishable_id',$item->attributes['cartable_id'])
                        ->where('wishable_type',$item->attributes['cartable_type'])
                        ->where('user_id',Auth::id())->first();
                    if (!empty($wishlist))
                        $wishlist->delete();
                    DB::commit();
                    \Cart::clear();
                    \Cart::session(Auth::id())->clear();
                }
            } catch (\Exception $exception){
                DB::rollBack();
                return back()->with('message',['status' => 500,'type' => 'error', 'message' => $exception->getMessage()]);
            }
            return view('website.transactionsuccess'); die;
            return redirect(route('main.user.profile',Auth::user()->username).'#order')->with('swal',[
                'status' => 200,
                'icon' => 'success',
                'showConfirmButton' => false,
                'timer' => 2000,
                'message' => "Transaksi <b>{$transaction->transaction_cd}</b> telah berhasil! nikmati kursus-kursus premium lain hanya di gurusemua.com"]);

        }else {
            return back()->with('message',['status' => 406,'type' => 'error', 'message' => 'keranjang anda kosong silahkan pilih kursus terlebih dahulu']);
        }
    }



    /**
     * Midtrans notification handler.
     *
     * @param Request $request
     *
     * @return void
     */
    public function notificationHandler(Request $request)
    {
        $notif = new Veritrans_Notification();
        \DB::transaction(function() use($notif) {

            $transaction = $notif->transaction_status;
            $type = $notif->payment_type;
            $orderId = $notif->order_id;
            $fraud = $notif->fraud_status;
            $donation = Donation::findOrFail($orderId);

            if ($transaction == 'capture') {

                // For credit card transaction, we need to check whether transaction is challenge by FDS or not
                if ($type == 'credit_card') {

                    if($fraud == 'challenge') {
                        // TODO set payment status in merchant's database to 'Challenge by FDS'
                        // TODO merchant should decide whether this transaction is authorized or not in MAP
                        // $donation->addUpdate("Transaction order_id: " . $orderId ." is challenged by FDS");
                        $donation->setPending();
                    } else {
                        // TODO set payment status in merchant's database to 'Success'
                        // $donation->addUpdate("Transaction order_id: " . $orderId ." successfully captured using " . $type);
                        $donation->setSuccess();
                    }

                }

            } elseif ($transaction == 'settlement') {

                // TODO set payment status in merchant's database to 'Settlement'
                // $donation->addUpdate("Transaction order_id: " . $orderId ." successfully transfered using " . $type);
                $donation->setSuccess();

            } elseif($transaction == 'pending'){

                // TODO set payment status in merchant's database to 'Pending'
                // $donation->addUpdate("Waiting customer to finish transaction order_id: " . $orderId . " using " . $type);
                $donation->setPending();

            } elseif ($transaction == 'deny') {

                // TODO set payment status in merchant's database to 'Failed'
                // $donation->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is Failed.");
                $donation->setFailed();

            } elseif ($transaction == 'expire') {

                // TODO set payment status in merchant's database to 'expire'
                // $donation->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is expired.");
                $donation->setExpired();

            } elseif ($transaction == 'cancel') {

                // TODO set payment status in merchant's database to 'Failed'
                // $donation->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is canceled.");
                $donation->setFailed();

            }
        });

        return;
    }

}