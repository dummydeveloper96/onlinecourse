<?php

namespace App\Http\Controllers;

use App\News;
use Auth;
use DB;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->isAdminPage()) {
          $news = News::simplePaginate(10);
          return view('admin.news.list', compact('news'));
        }
        $news = News::simplePaginate(10);
        return view('website.news', compact('news'));
    }
    public function indexFrontend()
    {
      $news = News::simplePaginate(10);
      return view('website.news', compact('news'));
    }
    
    public function readNewsDetail ($id) {
      try {
        $news = News::find($id);
        $news->visit_count = ++$news->visit_count;
        $news->save();
        return view('website.news-detail', compact('news'));
      } catch (\Exception $exception) {
        return redirect()->back()->withErrors('Terjadi kesalahan. Mohon coba lagi');
      }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try {
        $news = new News();
        $messages = [
          'title.required' => 'Article must have a title',
          'slug.required' => 'Please specify the slug for this article',
          'desc.required' => 'Article must have content',
          'news_summary.required' => 'Please specify a summary of this article',
          'news_thumbnail' => 'Please put image thumbnail for this article'
        ];
        $validator = \Validator::make($request->all(), [
          'title' => 'required|max:255',
          'slug' => 'required',
          'desc' => 'required',
          'news_summary' => 'required',
          'news_thumbnail' => 'required'
        ], $messages);

        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput();
        }

        $news->title = $request->title;
        $news->slug = $request->slug;
        $news->desc = $request->desc;
        $news->author_id = Auth::user()->id;
        $news->news_summary = $request->news_summary;
        $news->visit_count = (int) 0;
        $news->save();
        $news->addMedia($request->news_thumbnail)
                ->toMediaCollection('news_thumbnail');
        $news = News::simplePaginate(10);
        return view('admin.news.list', compact('news'))->with('success', 'Berhasil membuat artikel!');
      } catch (\Exception $exception) {
        return redirect()->back()->withErrors('Gagal membuat artikel! Mohon cek kembali formnya.');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        try {
          return view('website.news-detail', compact('news'));
        } catch (\Exception $exception) {
          return redirect()->back()->withErrors('Artikel tidak ditemukan');
        }
    }

    public function newsDetail($id) {
      try {
        $news = News::find($id);
        $news->visit_count = (int) $news->visit_count + 1;
        $news->save();
        return view('website.news-detail', compact('news'));
      } catch (\Exception $exception) {
        return view('website.news')->with('status', 'Artikel tidak ditemukan.');
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
          $news = News::find($id);
          return view('admin.news.edit', compact('news'));
        } catch (\Exception $exception) {
          return redirect()->back()->withErrors('Artikel tidak ditemukan');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        try {
          $news->author_id = Auth::user()->id;
          $news->title = $request->title;
          $news->desc = $request->content;
          $news->slug = $request->slug;
          $news->save();
          $news->clearMediaCollection('news_thumbnail');
          $news->addMedia($request->news_thumbnail)
                  ->toMediaCollection('news_thumbnail');
          $news = News::simplePaginate(10);
          return view('admin.news.list', compact('news'))->with('success', 'Berhasil memperbaharui artikel');
        } catch (\Exception $exception) {
          return redirect()->back()->withErrors('Gagal memperbaharui artikel! Mohon cek formnya kembali.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try {
        $news = News::find($id);
        $news->delete();
        $news = News::simplePaginate(10);
        return view('admin.news.list', compact('news'))->with('success', 'Berhasil menghapus artikel');
      } catch (\Exception $exception) {
        return redirect()->back()->withErrors('Gagal menghapus artikel');
      }
    }

    public function search(Request $request) {
        try {
          //code...
          $news = News::where('title', 'LIKE', '%'.$request->search.'%')->simplePaginate(10);
          if (count($news) > 0) {
            return view('website.news', compact('news'));
          } else {
            $result = 'Pencarian tidak ditemukan';
            return view('website.news', compact('news', 'result'));
          }
        } catch (\Throwable $th) {
          //throw $th;
          dd($th);
          return redirect()->back()->withErrors('Terjadi Kesalahan. Mohon coba kembali.');
        }
    }
}
