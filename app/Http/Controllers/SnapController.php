<?php

namespace App\Http\Controllers;
use App\Helper;
use App\Mentor;
use App\StudentMentor;
use App\Transaction;
use App\TransactionDetail;
use App\UserLesson;
use App\Veritrans\Veritrans;
use App\Wishlist;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Veritrans\Midtrans;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SnapController extends Controller
{
    public function __construct()
    {
        Midtrans::$serverKey = config('services.midtrans.serverKey');
        Midtrans::$isProduction = config('services.midtrans.isProduction');
        Veritrans::$serverKey = config('services.midtrans.serverKey');
        Veritrans::$isProduction = config('services.midtrans.isProduction');
    }

    public function token() 
    {
        \DB::transaction(function(){
            $midtrans = new Midtrans;

            // Populate items
            $items = \Cart::session(Auth::id())->getContent();
            $item_details = [];
            foreach ($items as $item){
                $item_details[] = [
                    'id'       => $item->id,
                    'price'    => $item->price,
                    'quantity' => $item->quantity,
                    'name'     => $item->name
                ];
            }
            $cart = [
                'items' => $items,
                'total' => \Cart::getTotal()
            ];

            $user = Auth::user();
            $counter = DB::table('transactions')->latest('id')->first();
            $counter = empty($counter)? $counter+1 : intval($counter->id)+1;
            $transaction = new Transaction;
            $transaction->transaction_cd = "INV-GS-".date('dmY').$counter;
            $transaction->student_id = $user->id;
            $transaction->ttl_prc_discount = 0;
            $transaction->ttl_prc_tax = 0;
            $transaction->ttl_prc_net = \Cart::getTotal();
            $transaction->payment_status = Transaction::PAYMENT_STATUS_PENDING;

            $payload = [
                'transaction_details' => [
                    'order_id'      => $transaction->transaction_cd,
                    'gross_amount'  => $transaction->ttl_prc_net,
                ],
                'customer_details' => [
                    'first_name'    => $user->name,
                    'email'         => $user->email,
                    // 'phone'         => '08888888888',
                    // 'address'       => '',
                ],
                'item_details' => $item_details,
            ];
            try
            {
                $snap_token = $midtrans->getSnapToken($payload);
                $transaction->snap_token = $snap_token;
                $transaction->save();

                foreach ($items as $item){
                    $transactionDetail = new TransactionDetail;
                    $transactionDetail->transaction_id = $transaction->id;
                    $transactionDetail->transactionable_type = Mentor::class;
                    $transactionDetail->transactionable_id = $item->attributes['cartable_id'];
                    $transactionDetail->days_duration += 1;
                    $transactionDetail->price = $item->price;
                    $transactionDetail->save();

                }
                //return redirect($vtweb_url);
                echo $snap_token;
            }
            catch (Exception $e)
            {
                return $e->getMessage;
            }
        });

    }

    public function finish(Request $request)
    {
        $resultType = $request->input('result_type');
        $result = $request->input('result_data');
        $result = json_decode($result);
        try {
          $transaction = Transaction::where('transaction_cd', $result->order_id)->firstOrFail();
          $transaction->pdf_url = $result->pdf_url;
          $transaction->save();
        } catch (\Exception $e) {
          abort(404);
        }
        
        \Cart::session(Auth::id());
        \Cart::clear();
        \Cart::session(Auth::id())->clear();
        switch ($resultType){
            case 'success' :
                return view('website.transactionsuccess')->with('result',$result);
                break;
            case 'pending' :
                return view('website.transactionpending')->with('result',$result);
                break;
            case 'error' :
                return view('website.transactionfailed')->with('result',$result);
                break;
            default : break;
        }
    }

    public function notification()
    {
        $midtrans = new Veritrans;
        $json_result = file_get_contents('php://input');
        $result = json_decode($json_result);

        if($result){
        $notif = $midtrans->status($result->order_id);
        }

        error_log(print_r($result,TRUE));


        $notif_status = $notif->transaction_status;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;
        $fraud = $notif->fraud_status;
        $mTransaction = Transaction::where('transaction_cd',$order_id)->first();

        if ($notif_status == 'capture') {

            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card') {

                if($fraud == 'challenge') {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                    // $mTransaction->addUpdate("Transaction order_id: " . $orderId ." is challenged by FDS");
                    $mTransaction->setPending();
                } else {
                    // TODO set payment status in merchant's database to 'Success'
                    // $mTransaction->addUpdate("Transaction order_id: " . $orderId ." successfully captured using " . $type);
                    $mTransaction->setSuccess();
                }

            }

        } elseif ($notif_status == 'settlement') {

            // TODO set payment status in merchant's database to 'Settlement'
            // $mTransaction->addUpdate("Transaction order_id: " . $orderId ." successfully transfered using " . $type);
            $mTransaction->setSuccess();

        } elseif($notif_status == 'pending'){

            // TODO set payment status in merchant's database to 'Pending'
            // $mTransaction->addUpdate("Waiting customer to finish transaction order_id: " . $orderId . " using " . $type);
            $mTransaction->setPending();

        } elseif ($notif_status == 'deny') {

            // TODO set payment status in merchant's database to 'Failed'
            // $mTransaction->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is Failed.");
            $mTransaction->setFailed();

        } elseif ($notif_status == 'expire') {

            // TODO set payment status in merchant's database to 'expire'
            // $mTransaction->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is expired.");
            $mTransaction->setExpired();

        } elseif ($notif_status == 'cancel') {

            // TODO set payment status in merchant's database to 'Failed'
            // $mTransaction->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is canceled.");
            $mTransaction->setFailed();

        }
    }
}    