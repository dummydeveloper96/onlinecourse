<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Helper;
use App\Lesson;
use App\Mentor;
use App\Notifications\CommentNotification;
use App\User;
use App\UserLesson;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\MediaLibrary\Models\Media;

class MentorLessonController extends Controller
{
    public function show($mentor,$lesson){
        $mentor = Mentor::getMentorByUsername($mentor);
        $lesson = Lesson::where('slug',$lesson)->firstOrFail();
        if ($this->isAdminPage()) {
            return view('admin.lesson')->with('mentor', $mentor)->with('lesson',$lesson);
        }else{
            return view('website.lesson')->with('mentor', $mentor)->with('lesson',$lesson);
        }
    }

    public function create($username){
        $mentor = Mentor::getMentorByUsername($username);
        return view('admin.lesson.create')->with('mentor' ,$mentor);
    }
    public function upload($username){
        return "success";
    }
    public function store(Request $request,$username){
        $validatedData = $request->validate(Lesson::$createRules);
        $mentor = Mentor::getMentorByUsername($username);
        $total_lesson = Lesson::where('mentor_id', $mentor->mentor_id)->max('order');
        $lesson = new Lesson();
        $lesson->mentor_id = $mentor->mentor_id;
        $lesson->title = $validatedData['title'];
        $lesson->slug = $validatedData['slug'];
        $lesson->desc = $validatedData['desc'];
        $lesson->thumbnail_photo = 0;
        $lesson->desc = $validatedData['desc'];
        $lesson->lesson_video_dacast_id = $validatedData['lesson_video_dacast_id'];
        $lesson->duration = $validatedData['duration'];
        $lesson->lesson_video_url = '';
        $lesson->order = $total_lesson + 1;
//        $lesson->lesson_video_url = Helper::youtubeURLtoEmbedURL($validatedData['lesson_video_url']);

//        if ((isset($validatedData['lesson_video']) && !empty($validatedData['lesson_video']))){
//            $lesson->lessonable_type = Media::class;
//            $lesson->lessonable_id = 0;
//        }
//        else{
//            // else ini sebetulnya untuk yang quiz
//            $lesson->lessonable_type = Media::class;
//            $lesson->lessonable_id = 0;
//        }

        $lesson->lessonable_type = Media::class;
        $lesson->lessonable_id = 0;
        // dd($lesson);
        DB::transaction(function () use ($lesson, $validatedData){
            $lesson->save();
            if (isset($validatedData['thumbnail_photo']) && !empty($validatedData['thumbnail_photo'])){
                $lesson->addMedia($validatedData['thumbnail_photo'])
                    ->toMediaCollection(Lesson::LESSON_MEDIA_THUMBNAIL);
                $lesson->thumbnail_photo = $lesson->media->last()->id;
            }
//            if (isset($validatedData['lesson_video']) && !empty($validatedData['lesson_video'])){
//                $lesson->addMedia($validatedData['lesson_video'])
//                    ->toMediaCollection(Lesson::LESSON_MEDIA_TYPE_VIDEO);
//                $lesson->lessonable_id = $lesson->media->last()->id;
//            }
            $lesson->save();;
        });

        return redirect()->route('mentors.show',$mentor->user->username)->with('success', 'Mentor is successfully saved');
    }

    public function edit($username, $id){
        $mentor = Mentor::getMentorByUsername($username);
        $lesson = Lesson::find($id);
        $lessonMedia = [];
        $lessonMedia['thumbnailPhoto'] = $lesson->getFirstMediaUrl(Lesson::LESSON_MEDIA_THUMBNAIL);
//        $lessonMedia['lessonVideo'] = $this->model->getFirstMediaUrl('lesson-video');
        return view('admin.lesson.edit')
            ->with('mentor' ,$mentor)
            ->with('lesson',$lesson)
            ->with('lessonMedia',$lessonMedia);
    }

    public function update(Request $request, $username, $id)
    {
        $updateRules = Lesson::$updateRules;
        $updateRules['slug'] .= ",$id,id,deleted_at,NULL";
        $validatedData = $request->validate($updateRules);
        $mentor = Mentor::getMentorByUsername($username);

        $lesson = Lesson::find($id);
        $lesson->title = $validatedData['title']?: $lesson->title;
        $lesson->slug = $validatedData['slug'] ?: $lesson->slug;
        $lesson->desc = $validatedData['desc'] ?: $lesson->desc;
        $lesson->lesson_video_dacast_id = $validatedData['lesson_video_dacast_id'];
        $lesson->duration = $validatedData['duration'];
        $lesson->lesson_video_url = '';
//        $lesson->lesson_video_url = Helper::youtubeURLtoEmbedURL($validatedData['lesson_video_url']);
        $lesson->lessonable_type = Media::class;

        DB::transaction(function () use ($lesson, $validatedData){
            $lesson->save();
            if (isset($validatedData['thumbnail_photo']) && !empty($validatedData['thumbnail_photo'])){
                $lesson->clearMediaCollection(Lesson::LESSON_MEDIA_THUMBNAIL);
                $lesson->addMedia($validatedData['thumbnail_photo'])
                    ->toMediaCollection(Lesson::LESSON_MEDIA_THUMBNAIL);
            }
//            if (isset($validatedData['lesson_video']) && !empty($validatedData['lesson_video'])){
//                $lesson->addMedia($validatedData['lesson_video'])
//                    ->toMediaCollection(Lesson::LESSON_MEDIA_TYPE_VIDEO);
//                $lesson->lessonable_id = $lesson->media->last()->id;
//            }
            $lesson->save();;
        });

        return back()->with('success', 'Mentor is successfully saved');
    }

    public function destroy($username, $id){
        $mentor = Mentor::getMentorByUsername($username);
        DB::transaction(function () use ($mentor, $id) {
            $lesson = Lesson::where('id',$id)->where('mentor_id',$mentor->mentor_id)->firstOrFail();
            $lesson->delete();

//            $notification = CommentNotification::where($id)->get();
        });

        return redirect()->route('mentors.show',$mentor->user->username)->with('success', 'Lesson is successfully deleted');
    }



    public function fetchComment($mentor, $lesson){
        $lessonId = Lesson::where('slug',$lesson)->firstOrFail()->id;
        $comments = DB::table('comments')
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->leftJoin('media', function($join){
                $join->on('media.model_id', '=', 'users.id')
                    ->where('media.model_type','=', User::class)
                    ->where('media.collection_name','=', 'avatar-photo');
            })
            ->where('comments.commentable_id','=',$lessonId)
            ->where('comments.commentable_type','=',Lesson::class)
            ->get(['comments.*','users.name','users.username',DB::raw("CONCAT('/storage','/',media.id,'/',media.file_name) as user_avatar")]);
        echo json_encode(['comments' => $comments, 'status' => 200]);
    }
    public function sendComment($mentor, $lesson){
        $lessonId = Lesson::where('slug',$lesson)->firstOrFail()->id;
        DB::transaction(function () use ($mentor, $lessonId) {
            $comment = new Comment();
            $comment->user_id = Auth::id();
            $comment->parent_id = $_POST['parent_id'] == 0 ? $_POST['reply_to'] : $_POST['parent_id'];
            $comment->reply_to = $_POST['reply_to'];
            $comment->text = $_POST['text'];
            $comment->commentable_id = $lessonId;
            $comment->commentable_type = Lesson::class;
            $comment->save();

            $lesson = Lesson::find($lessonId);
            $notificationActionUrl = route('main.mentors.lessons.show',[
                    'mentor' => $lesson->mentor->user->username,
                    'lesson' => $lesson->slug
                ])."#comment-".$comment->id;
            $lesson->mentor->user->notify(new CommentNotification($comment, $notificationActionUrl));
            $parentComment = Comment::with('user')->find($comment->parent_id);
            $replyToComment = Comment::with('user')->find($comment->reply_to);

            $doNotNotifToUserId = [Auth::id()];
            if (!in_array($lesson->mentor->user->id, $doNotNotifToUserId)){
                $doNotNotifToUserId[] = $lesson->mentor->user->id;
            }
            if (!empty($parentComment) && !in_array($parentComment->user->id, $doNotNotifToUserId)){
                $parentComment->user->notify(new CommentNotification($comment, $notificationActionUrl));
                $doNotNotifToUserId[] = $parentComment->user->id;
            }
            if (!empty($replyToComment) && !in_array($replyToComment->user->id, $doNotNotifToUserId)) {
                $replyToComment->user->notify(new CommentNotification($comment, $notificationActionUrl));
                $doNotNotifToUserId[] = $replyToComment->user->id;
            }
            echo json_encode(['comments' => $comment, 'status' => 200]);
        });
    }
    public function editComment($mentor, $lesson){
        $lessonId = Lesson::where('slug',$lesson)->firstOrFail()->id;
        $comment = Comment::findOrFail($_POST['id']);
        $comment->text = $_POST['text'];
        $comment->save();
        echo json_encode(['comments' => $comment, 'status' => 200]);
    }

    public function deleteComment($mentor, $lesson){
        $comment = Comment::where('id',$_POST['id'])->first();
        $comment->delete();
        echo json_encode(['comments' => $comment, 'status' => 200]);
    }

    public function finishWatching($mentor, $lesson){
        $mentor = Mentor::getMentorByUsername($mentor);
        $lessonId = Lesson::where('slug',$lesson)->firstOrFail()->id;
        $userLesson = UserLesson::where('user_id',Auth::id())
            ->where('lesson_id',$lessonId)->firstOrFail();
        $userLesson->watch_count = $userLesson->watch_count + 1;
        $userLesson->is_done_watching = Helper::STATUS_YES;
        $userLesson->save();
        return response(['message'=>'success']);
    }
}
