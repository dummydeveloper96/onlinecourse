<?php

namespace App\Http\Controllers;

use App\Freecourse;
use App\Helper;
use App\Mentor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FreecourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->isAdminPage()) {
            $freeCourses = Freecourse::all();
            return view('admin.freecourse.list',compact('freeCourses'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->isAdminPage()) {
            $mentors = Mentor::all();
            return view('admin.freecourse.create')->with('mentors',$mentors);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate(Freecourse::rules('create'));
        $freeCourse = new Freecourse();
        $freeCourse->title = $validatedData['title'];
        $freeCourse->mentor_name = $validatedData['mentor_name'];
        $freeCourse->desc = $validatedData['desc'];
        $freeCourse->video_url = Helper::youtubeURLtoEmbedURL($validatedData['video_url']);
        DB::transaction(function () use ($freeCourse, $validatedData){
            $freeCourse->save();
            if (isset($validatedData['thumbnail_photo']))
            {
                $freeCourse->clearMediaCollection(Freecourse::FREECOURSE_MEDIA_THUMBNAIL);
                $freeCourse->addMedia($validatedData['thumbnail_photo'])->toMediaCollection(Freecourse::FREECOURSE_MEDIA_THUMBNAIL);
            }
        });
        return redirect()->route('freecourses.index')->with('success', 'Free Course is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Freecourse  $freecourse
     * @return \Illuminate\Http\Response
     */
    public function show(Freecourse $freecourse)
    {
        if ($this->isAdminPage()) {
            $mentors = Mentor::all();
            return view('admin.freecourse.edit')
                ->with('course',$freecourse)
                ->with('mentors',$mentors);
        }else{
            $mentor = Mentor::where('mentor_id',$freecourse->mentor_id)->first();
            return view('website.freecourse')
                ->with('course',$freecourse)
                ->with('mentor',$mentor);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Freecourse  $freecourse
     * @return \Illuminate\Http\Response
     */
    public function edit(Freecourse $freecourse)
    {
        if ($this->isAdminPage()) {
            $mentors = Mentor::all();
            return view('admin.freecourse.edit')
                ->with('course',$freecourse)
                ->with('mentors',$mentors);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Freecourse  $freecourse
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Freecourse $freecourse)
    {
        $validatedData = $request->validate(Freecourse::rules('update'));
        $freecourse->title = $validatedData['title'];
        $freecourse->mentor_name = $validatedData['mentor_name'];
        $freecourse->desc = $validatedData['desc'];
        $freecourse->video_url = Helper::youtubeURLtoEmbedURL($validatedData['video_url']);
        DB::transaction(function () use ($freecourse, $validatedData){
            $freecourse->save();
            if (isset($validatedData['thumbnail_photo']))
            {
                $freecourse->clearMediaCollection(Freecourse::FREECOURSE_MEDIA_THUMBNAIL);
                $freecourse->addMedia($validatedData['thumbnail_photo'])->toMediaCollection(Freecourse::FREECOURSE_MEDIA_THUMBNAIL);
            }
        });
        return redirect()->route('freecourses.edit',$freecourse->id)->with('success', 'Free Course is successfully saved');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Freecourse  $freecourse
     * @return \Illuminate\Http\Response
     */
    public function destroy(Freecourse $freecourse)
    {
        $freecourse->delete();
        return redirect()->route('freecourses.index')->with('success', 'successfully delete freecourse');

    }
}
