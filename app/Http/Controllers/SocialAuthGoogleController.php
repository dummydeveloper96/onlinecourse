<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Laravel\Socialite\Two\InvalidStateException;
use Socialite;
use Auth;
use Exception;



class SocialAuthGoogleController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callback()
    {
        try {
            $googleUser = Socialite::driver('google')->user();
        } catch (InvalidStateException $e) {
            $googleUser = Socialite::driver('google')->stateless()->user();
        }

        $existUser = User::where('email',$googleUser->email)->first();

        if($existUser) {
            if(empty($existUser->email_verified_at)){
                $existUser->email_verified_at = now();
                $existUser->save();
            }
            Auth::loginUsingId($existUser->id);
        }
        else {
            $user = new User;
            $user->name = $googleUser->name;
            $user->email = $googleUser->email;
            $user->username = str_replace(' ', '', $googleUser->name).rand(1,99);
            $user->email_verified_at = now();
            $user->google_id = $googleUser->id;
            $user->password = bcrypt(rand(1,99999));
            $user->save();

            $user->assignRole('student');


            Auth::loginUsingId($user->id);
        }
        return redirect()->route('landingpage');
    }
}