<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Helper;
use App\Lesson;
use App\Mentor;
use App\MentorRating;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\MediaLibrary\Models\Media;

class MentorController extends Controller
{
    public function index()
    {
        if ($this->isAdminPage()) {
            $mentors = Mentor::all();
            return view('admin.mentor.list',compact('mentors'));
        }else{
            if (isset($_GET['sort_by'])){
                $orderBy = ['sort_by' => null, 'direction' => 'desc'];
                if ($_GET['sort_by'] == 'terbaru'){
                    $orderBy = ['sort_by' => 'updated_at', 'direction' => 'desc'];
                }
                else if ($_GET['sort_by'] == 'terlaku'){
                    $orderBy = ['sort_by' => 'transaction_count', 'direction' => 'desc'];
                }
                else if ($_GET['sort_by'] == 'termurah'){
                    $orderBy = ['sort_by' => 'price', 'direction' => 'asc'];
                }
                else if ($_GET['sort_by'] == 'termahal'){
                    $orderBy = ['sort_by' => 'price', 'direction' => 'desc'];
                }
                else if ($_GET['sort_by'] == 'rating terbaik'){
                    $orderBy = ['sort_by' => 'rating_avg', 'direction' => 'desc'];
                }
            }
            $mentors = Mentor::orderBy($orderBy['sort_by'] ?? 'rating_avg', $orderBy['direction'] ?? 'desc')->paginate(12);
            $mentorPilihan = Mentor::orderBy('rating_avg', 'desc')->first();
            return view('website.mentors',compact('mentors'))->with('mentorPilihan',$mentorPilihan);
        }
    }

    public function create()
    {
        return view('admin.mentor.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate(Mentor::$createRules);
        $user = new User();
        $user->name = $validatedData['name'];
        $user->username = $validatedData['username'];
        $user->email = $validatedData['email'];
        $user->gender = $validatedData['gender'];
        $user->password = Hash::make($validatedData['password']);

        $mentor = new Mentor();
        $mentor->profesi = $validatedData['profesi'];
        $mentor->department = $validatedData['department'];
        $mentor->desc = $validatedData['desc'];
        $mentor->price = $validatedData['price'];
        $mentor->mentor_video_url = Helper::youtubeURLtoEmbedURL($validatedData['mentor_video_url']);

        DB::transaction(function () use ($user, $mentor, $validatedData){
            $user->save();
            $user->assignRole('mentor');
            $mentor->mentor_id = $user->id;
            $mentor->save();

            $mentor->addMedia($validatedData['primary_photo'])
                ->toMediaCollection('primary-photo');
            $mentor->addMedia($validatedData['description_photo'])
                ->toMediaCollection('description-photo');
            /*
            $mentor->addMedia($validatedData['highlight_video'])
                ->toMediaCollection('highlight-video');
            */
        });
        return redirect()->route('mentors.index')->with('success', 'Mentor is successfully saved');
    }

    public function show($username)
    {
        $this->model = Mentor::getMentorByUsername($username);
        $lessons = Lesson::where('mentor_id',$this->model->mentor_id)->orderBy('order')->get();
        if ($this->isAdminPage()) {
            return view('admin.mentor.show')->with('mentor',$this->model)->with('lessons',$lessons);
        }else{
            return view('website.mentor')
              ->with('mentor',$this->model)
              ->with('lessons',$lessons)
              ->with('totalTransaction', $this->model->getTotalTransaction())
              ->with('bestRating', $this->model->getBestRating());
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $this->model = Mentor::find($id);
        $user = User::find($this->model->mentor_id);
        $mentorMedia = [];
        $mentorMedia['avatarPhoto'] = $user->getFirstMediaUrl('avatar-photo');
        $mentorMedia['primaryPhoto'] = $this->model->getFirstMediaUrl('primary-photo');
        $mentorMedia['descriptionPhoto'] = $this->model->getFirstMediaUrl('description-photo');
        $mentorMedia['highlightVideo'] = $this->model->getFirstMediaUrl('highlight-video');
        return view('admin.mentor.edit')
            ->with('mentor',$this->model)
            ->with('mentorMedia',$mentorMedia);
    }

    public function update(Request $request, $id)
    {
        $updateRules = Mentor::$updateRules;
        $updateRules['email'] .= ',id,' . $id;
        $updateRules['username'] .= ',id,' . $id;
        $validatedData = $request->validate($updateRules);

        $user = User::find($id);
        $user->name = $validatedData['name'] ?: $user->name ;
        $user->email = $validatedData['email'] ?: $user->email ;
        $user->username = $validatedData['username'] ?: $user->username ;
        $user->gender = $validatedData['gender'] ?: $user->gender ;

        $mentor = Mentor::find($id);
        $mentor->profesi = $validatedData['profesi'] ?: $mentor->profesi ;
        $mentor->desc = $validatedData['desc'] ?: $mentor->desc ;
        $mentor->price = $validatedData['price'] ?: $mentor->price ;
        $mentor->department = $validatedData['department'] ?: $mentor->department ;
        $mentor->mentor_video_url = Helper::youtubeURLtoEmbedURL($validatedData['mentor_video_url']);
        DB::transaction(function () use ($user ,$mentor, $validatedData){
            $user->save();
            $user->assignRole('mentor');
            $mentor->save();

            if (isset($validatedData['primary_photo']))
            {
                $mentor->clearMediaCollection('primary-photo');
                $mentor->addMedia($validatedData['primary_photo'])->toMediaCollection('primary-photo');
            }

            if (isset($validatedData['description_photo']))
            {
                $mentor->clearMediaCollection('description-photo');
                $mentor->addMedia($validatedData['description_photo'])->toMediaCollection('description-photo');
            }

            /*
            if (isset($validatedData['highlight_video']))
                $mentor->addMedia($validatedData['highlight_video'])->toMediaCollection('highlight-video');
            */
        });
        return redirect()->back()
            ->with('success', 'Mentor is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mentor  $mentor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mentor $mentor)
    {
        $mentor->delete();
        return redirect()->route('mentors.index')->with('success', 'Mentor is successfully deleted');
    }

    public function rearrangeLesson(Request $request,$id){
        $arrayID = explode(",", $request->inpSortedLesson);
        DB::beginTransaction();
        try{
            $mentor = Mentor::getMentorByUsername($id);
            foreach ($arrayID as $i => $lesson_id) {
                $lesson = Lesson::where('id', $lesson_id)->where('mentor_id', $mentor->mentor_id)->first();
                if (empty($lesson)){
                    DB::rollBack();
                    return redirect()->back()->with('error', 'Failed rearrange lesson, lesson unknown');
                }
                $lesson->order = $i + 1;
                $lesson->save();
            }
            DB::commit();
            return redirect()->back()
                ->with('success', 'Mentor is successfully updated');
        }
        catch(\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'Failed rearrange lesson, lesson unknown');
        }
    }

    public function selectReview(Request $request,$id){
        DB::beginTransaction();
            $mentor = Mentor::getMentorByUsername($id);
            $ratings = MentorRating::where('mentor_id',$mentor->mentor_id)->get();
            foreach ($ratings as $i => $rating){
                $rating->is_show = 0;
                $rating->save();
            }
            $rating = MentorRating::where('mentor_id',$mentor->mentor_id)->where('user_id',$request->rating[0])->first();
            $rating->is_show = 1;
            $rating->save();
            DB::commit();
            return redirect()->back()
                ->with('success', 'Mentor is successfully updated');
    }

    public function sendComment(Request $request,$username)
    {
        $mentor = Mentor::getMentorByUsername($username);
        $request['user_id'] = Auth::id();
        $validatedData = $request->validate(Comment::$createRules);

        // prevent comment 1 menit sekali

        $comment = Comment::findOrNew($request->comment_id);
        $comment->user_id = $validatedData['user_id'];
        $comment->parent_id = $validatedData['parent_id'];
        $comment->text = $validatedData['text'];

        DB::transaction(function () use ($comment, $mentor, $request){
            $comment->save();
            $mentor->comments()->save($comment);

            // Send Notification
            // Send to mentor and parent comment owner

            echo json_encode(['status' => 'success', 'msg' => "Successfully ". $request->comment_id == 0 ? 'send' : 'update' ." comment"]); die();
        });
    }
    public function sendRating(Request $request, $username){
        $mentor = Mentor::getMentorByUsername($username);
        $request['is_show'] = false;
        $validatedData = $request->validate(MentorRating::$createRules);

        $ratting = MentorRating::where('user_id',Auth::id())->where('mentor_id',$mentor->mentor_id)->first();
        if (empty($ratting)){
            $ratting = new MentorRating();
            $ratting->user_id = Auth::id();
            $ratting->mentor_id = $mentor->mentor_id;
            $ratting->testimonial = $validatedData['testimonial'];
            $ratting->rating = $validatedData['rating'];
            $ratting->is_show = 0;
            DB::transaction(function () use ($ratting, $mentor, $request){
                $ratting->save();
                $mentor->rating_avg = $mentor->ratings()->avg('rating');
                $mentor->save();
                echo json_encode(['status' => 'success', 'msg' => "Terimakasih telah memberi respon"]);
            });
        }else{
            echo json_encode(['status' => 'error', 'msg' => "Maaf anda hanya bisa memberi respon satu kali setiap mentor"]); die();
        }
    }
}
