<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements HasMedia, MustVerifyEmail
{
    use HasMediaTrait;
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    const TRIAL_TIME = '2 seconds'; // string to time

    public static $genders = ['-','laki-laki','perempuan'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static $updateRules = [
        'name' => 'required|max:255',
        'gender' => 'required|numeric',
        'avatar_photo' => 'image',
    ];

    public function mentor()
    {
        return $this->hasOne('App\Mentor');
    }

    public function supportDesk() {
      return $this->hasMany('App\SupportDesk', 'author');
    }

    public function news () {
      return $this->hasMany('App\News', 'author_id');
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('avatar-photo')
            ->singleFile();
    }

    public static function findByUsername($username){
        return self::where('username',$username)->firstOrFail();
    }

    public function hasMentorLesson($mentor_id){
        $mentor = Mentor::find($mentor_id);
        $mentorLesson = $mentor->lessons[0];
        $userLesson = UserLesson::where('user_id',$this->id)
            ->where('lesson_id',$mentorLesson->lesson_id)->first();
        return !empty($userLesson);
    }

    public function getTxtGenderAttribute($value){
        $value = $value?:0;
        return self::$genders[$value];
    }

    public function transactions(){
        return $this->hasMany(Transaction::class,'student_id');
    }

    public function hasMentor($mentor_id){
        $hasMentor = DB::select("Select *
            FROM student_mentors
            WHERE student_mentors.mentor_id = :mid
            AND student_mentors.student_id = :uid
            AND student_mentors.expired_dt > :now
            LIMIT 1
            ",[
                $mentor_id,
                $this->id,
                date('Y-m-d H:i:s'),
            ]);
        return !empty($hasMentor);
    }

    public function isTrial(){
        return $this->created_at > date('Y-m-d',strtotime('today - '.self::TRIAL_TIME));
    }

    public function student_mentors()
    {
        return $this->belongsToMany(StudentMentor::class, 'student_mentors', "student_id","mentor_id");
    }
//    public function myMentors(){
//
//        $mentors = DB::select("SELECT *
//            FROM mentors
//            JOIN users u on mentors.mentor_id = u.id
//            JOIN student_mentors sm ON mentors.mentor_id = sm.mentor_id
//            WHERE sm.student_id = ?",[Mentor::class,Auth::id()]);
//        return $mentors;
//    }
    public function myCourses(){
        $courses = DB::select("SELECT * 
            FROM mentors
            JOIN lessons ON mentors.mentor_id = lessons.mentor_id
            JOIN user_lessons ul on lessons.id = ul.lesson_id
                AND ul.user_id = ?",[Auth::id()]);
        return $courses;
    }
    public function isFinishWatching($lessonId){
        return UserLesson::where('lesson_id',$lessonId)->where('user_id',$this->id)->where('is_done_watching',Helper::STATUS_YES)->first();
    }


}
