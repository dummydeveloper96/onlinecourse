<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Lesson;
use Illuminate\Support\Facades\DB;


use Validator;
use Log;

class LessonQuiz extends Model
{
    //var for Table common attribure
    protected $table = 'lesson_quiz';
    protected $primaryKey = 'id';
    public $timestamps = false;

    //var for helpers
    protected $commonHelpers;

    //others
    protected $params;

    //appUser
    protected $loggedInUser;

    /*
    empty guard means fillable all
    */
    protected $guarded = [];

    public function setLoggedInUser($loggedInUser)
    {
        $this->loggedInUser = $loggedInUser;
    }

    public function getLoggedInUser()
    {
        return $this->loggedInUser;
    }

    public function setParams($params)
    {
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function create()
    {
        $data = array();
        $data['lesson_id'] = $this->params['lesson_id'];
        $data['title'] = $this->params['title'];
        $data['status'] = 10;

        $lesson = new Lesson();
        $lesson = $lesson->where('id', $data['lesson_id'] );
        if (empty($lesson)) {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Lesson Not Found\'']);
        }

        DB::beginTransaction();

        try {

            $created_lesson = $this->create($data);
            DB::commit();

        }

        catch (\Exception $e) {

            /* QUERY FAILED AND ROLL BACK ALL THE INSERT DATA */
            DB::rollback();

            $response['code'] = 500;
            $response['status'] = "error";
            $response['messages'] = $e->getMessage();
            return response()->json(['status' => 500,'type' => 'error', 'message' => $e->getMessage()]);
        }

        return $created_lesson;
    }

    public function update()
    {
        $data = array();
        $data['id'] = $this->params['quiz_id'];
        $data['lesson_id'] = $this->params['lesson_id'];
        $data['title'] = $this->params['title'];
        $data['status'] = 10;

        $lesson = new Lesson();
        $lesson = $lesson->where('id', $data['lesson_id'] );
        if (empty($lesson)) {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Lesson Not Found\'']);
        }

        $quiz = $this->where('id', $data['id'])->first();
        if (empty($quiz)) {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Quiz Not Found\'']);
        }

        DB::beginTransaction();

        try {

            $updateData = $data;
            foreach ($updateData as $quizKey => $quizdataValue) {
                $quiz->{$quizKey} = $quizdataValue;
            }

            $quiz->save();
            DB::commit();

        }

        catch (\Exception $e) {

            /* QUERY FAILED AND ROLL BACK ALL THE INSERT DATA */
            DB::rollback();

            $response['code'] = 500;
            $response['status'] = "error";
            $response['messages'] = $e->getMessage();
            return response()->json(['status' => 500,'type' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function getDetailsQuizByID($quiz_id)
    {
        $quiz = $this->where('id',$quiz_id)->first();

        if(empty($quiz))
        {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Quiz Not Found\'']);
        }

        return $quiz;

    }

    public function getDetailsQuizByLessonID($lesson_id)
    {
        $quiz = $this->where('lesson_id',$lesson_id)->first();

        if(empty($quiz))
        {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Quiz Not Found\'']);
        }

        return $quiz;

    }

    public function getQuizListByMentorID($mentor_id)
    {
        $quiz = $this->join('lessons', 'lessons.id','=','lesson_quiz.lesson_id')
        ->where('lessons.mentor_id',$mentor_id)->first();

        if(empty($quiz))
        {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Quiz Not Found\'']);
        }

        return $quiz;

    }
}
