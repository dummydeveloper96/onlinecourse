<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Lesson;
use Illuminate\Support\Facades\DB;


use Validator;
use Log;

class QuestionAnswer extends Model
{
    //var for Table common attribure
    protected $table = 'question_answer';
    protected $primaryKey = 'id';
    public $timestamps = false;

    //var for helpers
    protected $commonHelpers;

    //others
    protected $params;

    //appUser
    protected $loggedInUser;

    /*
    empty guard means fillable all
    */
    protected $guarded = [];

    public function setLoggedInUser($loggedInUser)
    {
        $this->loggedInUser = $loggedInUser;
    }

    public function getLoggedInUser()
    {
        return $this->loggedInUser;
    }

    public function setParams($params)
    {
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function create()
    {
        $data = array();
        $data['quiz_questions_id'] = $this->params['quiz_questions_id'];
        $data['answer'] = $this->params['answer'];
        $data['choice_word'] = $this->params['choice_word'];
        $data['is_answer'] = $this->params['is_answer'];
        $data['status'] = 10;

        $question = new QuizQuestion();
        $question = $question->where('id', $data['quiz_questions_id'] );
        if (empty($lesson)) {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Question Not Found\'']);
        }

        DB::beginTransaction();

        try {

            $created_answer = $this->create($data);
            DB::commit();

        }

        catch (\Exception $e) {

            /* QUERY FAILED AND ROLL BACK ALL THE INSERT DATA */
            DB::rollback();

            $response['code'] = 500;
            $response['status'] = "error";
            $response['messages'] = $e->getMessage();
            return response()->json(['status' => 500,'type' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function update()
    {
        $data = array();
        $data['id'] = $this->params['answer_id'];
        $data['quiz_questions_id'] = $this->params['quiz_questions_id'];
        $data['answer'] = $this->params['answer'];
        $data['choice_word'] = $this->params['choice_word'];
        $data['is_answer'] = $this->params['is_answer'];
        $data['status'] = 10;

        $question = new QuizQuestion();
        $question = $question->where('id', $data['quiz_questions_id'] );
        if (empty($lesson)) {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Question Not Found\'']);
        }

        $answer = $this->where('id', $data['id'])->first();
        if (empty($answer)) {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Answer Not Found\'']);
        }

        DB::beginTransaction();

        try {

            $updateData = $data;
            foreach ($updateData as $answerKey => $answerKeyDataValue) {
                $answer->{$answerKey} = $answerKeyDataValue;
            }

            $answer->save();
            DB::commit();

        }

        catch (\Exception $e) {

            /* QUERY FAILED AND ROLL BACK ALL THE INSERT DATA */
            DB::rollback();

            $response['code'] = 500;
            $response['status'] = "error";
            $response['messages'] = $e->getMessage();
            return response()->json(['status' => 500,'type' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function geDetailAnswerByID($answer_id)
    {
        $answer = $this->where('id',$answer_id)->first();

        if(empty($answer))
        {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Answer Not Found\'']);
        }

        return $answer;

    }

    public function getAnswerByQuestionID($question_id)
    {
        $answer = $this->where('question_id',$question_id)->orderBy('choice_word')->get();

        if(empty($answer))
        {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Answer Not Found\'']);
        }

        return $answer;

    }
}
