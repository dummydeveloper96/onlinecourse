<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Lesson;
use Illuminate\Support\Facades\DB;


use Validator;
use Log;

class QuizQuestion extends Model
{
    //var for Table common attribure
    protected $table = 'quiz_questions';
    protected $primaryKey = 'id';
    public $timestamps = false;

    //var for helpers
    protected $commonHelpers;

    //others
    protected $params;

    //appUser
    protected $loggedInUser;

    /*
    empty guard means fillable all
    */
    protected $guarded = [];

    public function setLoggedInUser($loggedInUser)
    {
        $this->loggedInUser = $loggedInUser;
    }

    public function getLoggedInUser()
    {
        return $this->loggedInUser;
    }

    public function setParams($params)
    {
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function create()
    {
        $data = array();
        $data['quiz_id'] = $this->params['quiz_id'];
        $data['question'] = $this->params['question'];
        $data['order'] = $this->params['order'];
        $data['status'] = 10;

        $quiz = new LessonQuiz();
        $quiz = $quiz->where('id', $data['quiz_id'] );
        if (empty($quiz)) {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Quiz Not Found\'']);
        }

        DB::beginTransaction();

        try {

            $created_quiz = $this->create($data);
            DB::commit();

        }

        catch (\Exception $e) {

            /* QUERY FAILED AND ROLL BACK ALL THE INSERT DATA */
            DB::rollback();

            $response['code'] = 500;
            $response['status'] = "error";
            $response['messages'] = $e->getMessage();
            return response()->json(['status' => 500,'type' => 'error', 'message' => $e->getMessage()]);
        }

        return $created_quiz;
    }

    public function update()
    {
        $data = array();
        $data['id'] = $this->params['question_id'];
        $data['quiz_id'] = $this->params['quiz_id'];
        $data['question'] = $this->params['question'];
        $data['order'] = $this->params['order'];
        $data['status'] = 10;

        $quiz = new LessonQuiz();
        $quiz = $quiz->where('id', $data['quiz_id'] );
        if (empty($quiz)) {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Quiz Not Found\'']);
        }

        $question = $this->where('id', $data['id'])->first();
        if (empty($question)) {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Question Not Found\'']);
        }

        DB::beginTransaction();

        try {

            $updateData = $data;
            foreach ($updateData as $questionKey => $questionDataValue) {
                $question->{$questionKey} = $questionDataValue;
            }

            $quiz->save();
            DB::commit();

        }

        catch (\Exception $e) {

            /* QUERY FAILED AND ROLL BACK ALL THE INSERT DATA */
            DB::rollback();

            $response['code'] = 500;
            $response['status'] = "error";
            $response['messages'] = $e->getMessage();
            return response()->json(['status' => 500,'type' => 'error', 'message' => $e->getMessage()]);
        }
    }

    public function getDetailsQuestionByID($question_id)
    {
        $question = $this->where('id',$question_id)->first();

        if(empty($question))
        {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Question Not Found\'']);
        }

        return $question;

    }

    public function getQuestionByQuizID($quiz_id)
    {
        $questions = $this->where('quiz_id',$quiz_id)->get();

        if(empty($questions))
        {
            return response()->json(['status' => 400,'type' => 'error', 'message' => 'Questions Not Found\'']);
        }

        return $questions;

    }
}
