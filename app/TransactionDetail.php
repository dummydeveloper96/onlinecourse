<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{

    protected $with = ['transactionable'];
    public function transactionable()
    {
        return $this->morphTo()->withTrashed();
    }
}
