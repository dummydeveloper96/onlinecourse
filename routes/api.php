<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//api
Route::group(['prefix' => 'api'], function() {
    Route::group(['prefix' => 'lesson'], function() {
        Route::group(['prefix' => 'quiz'], function() {
//            Route::post('v1/create', 'API\LessonQuizController@create');
        });
    });
});
